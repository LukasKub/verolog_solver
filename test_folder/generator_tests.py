import unittest
from utils import description
from source import generator


class GeneratorTestCase(unittest.TestCase):

    def test001_null_generation(self):
        generated_null_solution = generator._generate_null_solution()

        self.assertTrue(not generated_null_solution.journeys, "No journeys should be generated in null solution")

    def test002_base_solution_generation(self):
        problem_desc = description.ProblemDescription()
        problem_desc.requests = [description.Request(1, 2, -1, -1, 1, 5)]
        problem_desc.locations = [description.Location(1, 10, 10), description.Location(2, 20, 20)]
        problem_desc.machines = [description.Machine(1, 3)]

        generated_solution = generator.generate_solution(problem_desc)

        self.assertEqual(len(generated_solution.journeys), 1, "Generated wrong number of journey")
        self.assertEqual(generated_solution.journeys[0].path, [1, 2, 1], "Generated wrong path")
        self.assertEqual(generated_solution.get_requests(), [1], "Request getting failed")


if __name__ == '__main__':
    unittest.main()
