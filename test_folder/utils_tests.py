import unittest
from utils import utils, description


class CapacityCheckTestCase(unittest.TestCase):

    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.truck_capacity = 18
        self.problem_desc = problem_desc

    def test201_capacity_check_pure(self):
        self.problem_desc.requests = [
            description.Request(1, -1, -1, -1, 1, 1, self.problem_desc),
            description.Request(2, -1, -1, -1, 2, 1, self.problem_desc),
            description.Request(3, -1, -1, -1, 3, 1, self.problem_desc),
            description.Request(4, -1, -1, -1, 4, 1, self.problem_desc),
            description.Request(5, -1, -1, -1, 1, 5, self.problem_desc),
            description.Request(6, -1, -1, -1, 2, 2, self.problem_desc),
            description.Request(7, -1, -1, -1, 3, 2, self.problem_desc),
            description.Request(8, -1, -1, -1, 4, 1, self.problem_desc),
        ]

        journey_requests_list = [[1], [2], [3], [4], [5], [6], [7], [8]]

        for journey_requests in journey_requests_list:
            self.assertTrue(utils.check_capacity(journey_requests, self.problem_desc), "Failed capacity check")

    def test202_capacity_check_combined(self):
        self.problem_desc.requests = [
            description.Request(1, -1, -1, -1, 1, 1, self.problem_desc),
            description.Request(2, -1, -1, -1, 2, 1, self.problem_desc),
            description.Request(3, -1, -1, -1, 3, 1, self.problem_desc),
            description.Request(4, -1, -1, -1, 4, 1, self.problem_desc),
        ]

        journey_requests_list = [[1, 2], [1, 2, 3], [3, 4]]
        for journey_requests in journey_requests_list:
            self.assertTrue(utils.check_capacity(journey_requests, self.problem_desc), "Failed capacity check")

    def test203_capacity_check_equal(self):
        self.problem_desc.requests = [
            description.Request(1, -1, -1, -1, 1, 6, self.problem_desc),
            description.Request(2, -1, -1, -1, 2, 3, self.problem_desc),
            description.Request(3, -1, -1, -1, 3, 1, self.problem_desc),
            description.Request(4, -1, -1, -1, 4, 1, self.problem_desc),
        ]

        journey_requests_list = [[1], [2], [3, 4]]
        for journey_requests in journey_requests_list:
            self.assertTrue(utils.check_capacity(journey_requests, self.problem_desc), "Failed capacity check")

    def test204_capacity_check_over_flow(self):
        self.problem_desc.requests = [
            description.Request(1, -1, -1, -1, 1, 1, self.problem_desc),
            description.Request(2, -1, -1, -1, 2, 1, self.problem_desc),
            description.Request(3, -1, -1, -1, 3, 1, self.problem_desc),
            description.Request(4, -1, -1, -1, 4, 1, self.problem_desc),
            description.Request(5, -1, -1, -1, 1, 7, self.problem_desc),
            description.Request(6, -1, -1, -1, 2, 4, self.problem_desc),
        ]

        journey_requests_list = [[1, 2, 3, 4], [5], [6]]
        for journey_requests in journey_requests_list:
            self.assertFalse(utils.check_capacity(journey_requests, self.problem_desc, False), "Failed capacity check")


class JourneyLengthTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.problem_desc = description.ProblemDescription()
        self.problem_desc.truck_max_distance = 1000

    def test211_distance_check_pure(self):
        self.problem_desc.locations = [
            description.Location(1, 0, 0),
            description.Location(2, 5, 0),
            description.Location(3, 0, 5),
            description.Location(4, 3, 4),
            description.Location(5, 12, 5),
        ]

        self.assertEqual(utils.calculate_journey_length([2], self.problem_desc), 10)
        self.assertEqual(utils.calculate_journey_length([3], self.problem_desc), 10)
        self.assertEqual(utils.calculate_journey_length([4], self.problem_desc), 10)
        self.assertEqual(utils.calculate_journey_length([5], self.problem_desc), 26)

    def test212_distance_check_combined(self):
        self.problem_desc.locations = [
            description.Location(1, 0, 0),
            description.Location(2, 5, 0),
            description.Location(3, 8, 4),
            description.Location(4, 20, 9),
            description.Location(5, 12, 3),
            description.Location(6, 0, -2),
        ]

        self.assertEqual(utils.calculate_journey_length([2, 3, 4, 5, 6], self.problem_desc), 48)


class LocationGettingTestCase(unittest.TestCase):

    def test231_basic_location_getting(self):
        paths = [[1, 2, 4, 3, 1], [1, 6, 8, 4, 9, 7, 1], [1, 2, 1]]
        for path in paths:
            self.assertEqual(utils.get_locations_from_path(path), path[1:len(path)-1],
                             "Could not parse locations from path")

    def test232_location_gettng_edge_cases(self):
        paths = [[1, 2, 3, 4], [2, 3, 4, 1], [2, 3, 4]]
        for path in paths:
            self.assertEqual(utils.get_locations_from_path(path), [2, 3, 4],
                             "Could not properly parse reduced paths")


class PermutationsTestCase(unittest.TestCase):

    def test241_basic_permutations(self):
        paths = [
            [1],
            [1, 2],
            [1, 2, 1],
            [1, 2, 3],
        ]
        results = [
            [[1]],
            [[1, 2], [2, 1]],
            [[1, 1, 2], [1, 2, 1], [2, 1, 1]],
            [[1, 2, 3], [1, 3, 2], [3, 1, 2], [2, 1, 3], [2, 3, 1], [3, 2, 1]]
        ]
        for path, result in zip(paths, results):
            trial_result = utils._get_permutations(path)
            i = 0
            for combination in trial_result:
                if combination in result:
                    i += 1
            self.assertEqual(i, len(result), "Did not generate all combinations")


if __name__ == '__main__':
    unittest.main()
