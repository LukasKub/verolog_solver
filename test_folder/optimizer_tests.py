from utils import description
from source.local_search.request_movement import _move_request
from source.local_search.request_swap import _swap_requests
from source.local_search.helper_functions import assimilate_request, remove_request
from source.local_search.local_search_algorithms import hillclimb_check

from utils import utils
import unittest


class RequestMoveTestCase(unittest.TestCase):
    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.locations = [
                description.Location(1, 0, 0),
                description.Location(2, 5, 0),
                description.Location(3, 0, 5),
                description.Location(4, 30, 40),
            ]
        problem_desc.requests = [
                description.Request(1, 2, -1, -1, 1, 1, problem_desc),
                description.Request(2, 2, -1, -1, 1, 1, problem_desc),
                description.Request(3, 2, -1, -1, 1, 6, problem_desc),
                description.Request(3, 4, -1, -1, 1, 1, problem_desc),
            ]
        problem_desc.truck_capacity = 18
        problem_desc.truck_max_distance = 100
        problem_desc.truck_distance_cost = 100
        problem_desc.truck_day_cost = 1000
        problem_desc.truck_cost = 10000

        self.problem_desc = problem_desc

        self.journeys = [
            description.Journey(3, 10, [1, 2, 1], [1]),
            description.Journey(3, 10, [1, 2, 1], [2]),
            description.Journey(18, 10, [1, 2, 1], [3]),
            description.Journey(3, 100, [1, 4, 1], [4]),
        ]
        self.journey_count = len(self.journeys)

    def test101_basic_request_move(self):
        solution = description.Solution(self.journeys)
        new_solution = _move_request(solution, 1, 0, 0, self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count - 1, "Did not properly remove request")

    def test102_request_move_fail_cases(self):
        solution = description.Solution(self.journeys)
        new_solution = _move_request(solution, 2, 0, 0, self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Moved overweight request")

        new_solution = _move_request(solution, 0, 0, 2, self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Moved request to overweight journey")

        new_solution = _move_request(solution, 0, 0, 3, self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Moved request and made long journey")


class RequestSwitchTestCase(unittest.TestCase):
    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.requests = [
                description.Request(1, 2, -1, -1, 1, 1),
                description.Request(2, 2, -1, -1, 1, 1),
                description.Request(3, 3, -1, -1, 1, 1),
                description.Request(3, 4, -1, -1, 1, 1),
            ]
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.locations = [
                description.Location(1, 0, 0),
                description.Location(2, 5, 0),
                description.Location(3, 0, 5),
                description.Location(4, 30, 40),
            ]
        problem_desc.truck_capacity = 18
        problem_desc.truck_max_distance = 100
        problem_desc.truck_distance_cost = 100
        problem_desc.truck_day_cost = 1000
        problem_desc.truck_cost = 10000

        self.problem_desc = problem_desc

        self.journeys = [
            description.Journey(3, 10, [1, 2, 1], [1]),
            description.Journey(3, utils.calculate_journey_length([1, 2, 3, 1], self.problem_desc),
                                [1, 2, 3, 1], [2, 3]),
            description.Journey(3, 100, [1, 4, 1], [4]),
        ]
        self.journey_count = len(self.journeys)

    def test111_basic_request_switch(self):
        solution = description.Solution(self.journeys)
        new_solution = _swap_requests(solution, (1, 0), (0, 0), self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Did not properly switch requests")

        new_solution = _swap_requests(solution, (1, 1), (0, 0), self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Did not properly switch requests")

        new_solution = _swap_requests(solution, (2, 0), (0, 0), self.problem_desc)
        self.assertEqual(solution.get_requests(), new_solution.get_requests(), "Original and new requests are not equal")
        self.assertEqual(len(new_solution.journeys), self.journey_count, "Did not properly switch requests")


class AssimilationTestCase(unittest.TestCase):

    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.locations = [
                description.Location(1, 0, 0),
                description.Location(2, 5, 0),
                description.Location(3, 0, 5),
                description.Location(4, 6, 8),
            ]
        problem_desc.truck_capacity = 18
        problem_desc.truck_max_distance = 100
        problem_desc.truck_distance_cost = 100
        problem_desc.truck_day_cost = 1000
        problem_desc.truck_cost = 10000

        self.problem_desc = problem_desc

    def test121_assimilate_basic(self):
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 1, 1, self.problem_desc),
            description.Request(2, 2, -1, -1, 1, 1, self.problem_desc),
            description.Request(3, 2, -1, -1, 1, 1, self.problem_desc),
        ]

        journey = description.Journey(0, 0, [1, 1], [])
        for req in self.problem_desc.requests:
            journey = assimilate_request(journey, req.id, self.problem_desc)

        self.assertEqual(sorted(journey.requests), [1, 2, 3], "Not all requests have been added")

    def test122_assimilate_path(self):
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 1, 1, self.problem_desc),
            description.Request(2, 3, -1, -1, 1, 1, self.problem_desc),
            description.Request(3, 4, -1, -1, 1, 1, self.problem_desc),
        ]

        journey = description.Journey(0, 0, [1, 1], [])
        for req in self.problem_desc.requests:
            journey = assimilate_request(journey, req.id, self.problem_desc)

        self.assertTrue(
            journey.path in [[1, 2, 4, 3, 1], [1, 3, 4, 2, 1]],
            "Assimilated path is not shortest available"
        )

    def test123_assimilate_overweight(self):
        self.problem_desc.requests = [
            description.Request(1, -1, -1, -1, 1, 1),
            description.Request(2, -1, -1, -1, 2, 1),
            description.Request(3, -1, -1, -1, 3, 1),
            description.Request(4, -1, -1, -1, 4, 1),
            description.Request(5, -1, -1, -1, 1, 7),
            description.Request(6, -1, -1, -1, 2, 4),
        ]
        journeys = [
            description.Journey(utils.calculate_total_weight([1], self.problem_desc), 0, [], [1]),
            description.Journey(utils.calculate_total_weight([2], self.problem_desc), 0, [], [2]),
            description.Journey(utils.calculate_total_weight([3], self.problem_desc), 0, [], [3]),
            description.Journey(utils.calculate_total_weight([4], self.problem_desc), 0, [], [4]),
            description.Journey(utils.calculate_total_weight([5], self.problem_desc), 0, [], [5]),
            description.Journey(utils.calculate_total_weight([6], self.problem_desc), 0, [], [6]),
            description.Journey(utils.calculate_total_weight([1, 2], self.problem_desc), 0, [], [1, 2]),
            description.Journey(utils.calculate_total_weight([1, 2], self.problem_desc), 0, [], [1, 2]),
            description.Journey(utils.calculate_total_weight([1, 2], self.problem_desc), 0, [], [1, 2]),
            description.Journey(utils.calculate_total_weight([2, 3], self.problem_desc), 0, [], [2, 3]),
            description.Journey(utils.calculate_total_weight([2, 3], self.problem_desc), 0, [], [2, 3]),
            description.Journey(utils.calculate_total_weight([2, 3], self.problem_desc), 0, [], [2, 3]),
        ]
        requests = [
            5,
            6,
            5,
            6,
            1,
            2,
            4,
            5,
            6,
            4,
            5,
            6,
        ]

        for journey, request in zip(journeys, requests):
            self.assertEqual(assimilate_request(journey, request, self.problem_desc), -1,
                             "Assimilated overweight request")


class RequestRemovalTestCast(unittest.TestCase):

    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.locations = [
                description.Location(1, 0, 0),
                description.Location(2, 5, 0),
                description.Location(3, 0, 5),
                description.Location(4, 6, 8),
                description.Location(5, 60, 80),
            ]
        problem_desc.truck_max_distance = 100

        self.problem_desc = problem_desc

    def test131_test_removal(self):
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 2, 1),
            description.Request(2, 2, -1, -1, 2, 1),
            description.Request(3, 3, -1, -1, 3, 1),
            description.Request(4, 4, -1, -1, 4, 1),
        ]
        journeys = [
            description.Journey(
                utils.calculate_total_weight([1], self.problem_desc),
                utils.calculate_journey_length([1, 2, 1], self.problem_desc), [1, 2, 1], [1]),
            description.Journey(
                utils.calculate_total_weight([2], self.problem_desc),
                utils.calculate_journey_length([1, 2, 1], self.problem_desc), [1, 2, 1], [2]),
            description.Journey(
                utils.calculate_total_weight([3, 4], self.problem_desc),
                utils.calculate_journey_length([1, 3, 4, 1], self.problem_desc), [1, 3, 4, 1], [3, 4]),
            description.Journey(
                utils.calculate_total_weight([1, 2], self.problem_desc),
                utils.calculate_journey_length([1, 2, 2, 1], self.problem_desc), [1, 2, 2, 1], [1, 2]),
            description.Journey(
                utils.calculate_total_weight([1, 2, 3], self.problem_desc),
                utils.calculate_journey_length([1, 2, 2, 3, 1], self.problem_desc), [1, 2, 2, 3, 1], [1, 2, 3]),
        ]
        remove_requests = [
            1,
            2,
            3,
            1,
            1,
        ]
        requests = [
            [],
            [],
            [4],
            [2],
            [2, 3],
        ]

        for journey, remove_req, request in zip(journeys, remove_requests, requests):
            self.assertEqual(remove_request(journey, remove_req, self.problem_desc).requests, request,
                             "Did not properly remove request")

    def test132_removal_edge_cases(self):
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 2, 1),
            description.Request(2, 2, -1, -1, 2, 1),
            description.Request(3, 3, -1, -1, 3, 1),
            description.Request(4, 4, -1, -1, 4, 1),
            description.Request(5, 5, -1, -1, 4, 1),
        ]
        journeys = [
            description.Journey(
                utils.calculate_total_weight([1], self.problem_desc),
                utils.calculate_journey_length([1, 2, 1], self.problem_desc), [1, 2, 1], [1]),
            description.Journey(
                utils.calculate_total_weight([2], self.problem_desc),
                utils.calculate_journey_length([1, 2, 1], self.problem_desc), [1, 2, 1], [2]),
            description.Journey(
                utils.calculate_total_weight([3, 4], self.problem_desc),
                utils.calculate_journey_length([1, 3, 4, 1], self.problem_desc), [1, 3, 4, 1], [3, 4]),
            description.Journey(
                utils.calculate_total_weight([1, 2], self.problem_desc),
                utils.calculate_journey_length([1, 2, 2, 1], self.problem_desc), [1, 2, 2, 1], [1, 2]),
            description.Journey(
                utils.calculate_total_weight([1, 2, 3], self.problem_desc),
                utils.calculate_journey_length([1, 2, 2, 3, 1], self.problem_desc), [1, 2, 2, 3, 1], [1, 2, 3]),
            description.Journey(
                utils.calculate_total_weight([4, 5], self.problem_desc),
                utils.calculate_journey_length([1, 4, 5, 1], self.problem_desc), [1, 4, 5, 1],
                [4, 5]),

        ]
        remove_requests = [
            2,
            3,
            1,
            3,
            4,
        ]
        for journey, remove_req in zip(journeys, remove_requests):
            self.assertEqual(remove_request(journey, remove_req, self.problem_desc), -1,
                             "Did not fail while removing processing nonsense removal")


class ComparisonTestCase(unittest.TestCase):

    def setUp(self) -> None:
        problem_desc = description.ProblemDescription()
        problem_desc.machines = [
                description.Machine(1, 3),
                description.Machine(2, 6),
                description.Machine(3, 8),
                description.Machine(4, 10),
            ]
        problem_desc.locations = [
                description.Location(1, 0, 0),
                description.Location(2, 5, 0),
                description.Location(3, 0, 5),
                description.Location(4, 6, 8),
            ]
        problem_desc.truck_capacity = 18
        problem_desc.truck_max_distance = 100
        problem_desc.truck_distance_cost = 100
        problem_desc.truck_day_cost = 1000
        problem_desc.truck_cost = 10000

        self.problem_desc = problem_desc

    def test141_free_distance_comparison(self):
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 1, 1),
            description.Request(2, 2, -1, -1, 1, 1),
        ]
        self.problem_desc.truck_distance_cost = 0

        sol1 = description.Solution(
            [
                description.Journey(3, utils.calculate_journey_length([1, 2, 1], self.problem_desc),
                                    [1, 2, 1], [1]),
                description.Journey(3, utils.calculate_journey_length([1, 2, 1], self.problem_desc),
                                    [1, 2, 1], [2]),
            ]
        )
        sol2 = description.Solution(
            [
                description.Journey(3, utils.calculate_journey_length([1, 2, 2, 1], self.problem_desc),
                                    [1, 2, 2, 1], [1, 2]),
            ]
        )

        self.assertEqual(hillclimb_check(sol1, sol2, self.problem_desc), sol2,
                         "Null distance solution with more journeys should be worse")

    def test142_free_truck_comparison(self):
        self.problem_desc.truck_day_cost = 0
        self.problem_desc.truck_cost = 0
        self.problem_desc.requests = [
            description.Request(1, 2, -1, -1, 1, 1),
            description.Request(2, 3, -1, -1, 1, 1),
            description.Request(3, 4, -1, -1, 1, 1),
        ]

        sol1 = description.Solution(
            [
                description.Journey(3, utils.calculate_journey_length([1, 2, 1], self.problem_desc),
                                    [1, 2, 1], [1]),
                description.Journey(3, utils.calculate_journey_length([1, 3, 1], self.problem_desc),
                                    [1, 3, 1], [2]),
                description.Journey(3, utils.calculate_journey_length([1, 4, 1], self.problem_desc),
                                    [1, 4, 1], [3]),
            ]
        )

        sol2 = description.Solution(
            [
                description.Journey(6, utils.calculate_journey_length([1, 2, 3, 1], self.problem_desc),
                                    [1, 2, 3, 1], [1, 2]),
                description.Journey(3, utils.calculate_journey_length([1, 4, 1], self.problem_desc),
                                    [1, 4, 1], [3]),
            ]
        )

        sol3 = description.Solution(
            [
                description.Journey(6, utils.calculate_journey_length([1, 2, 4, 1], self.problem_desc),
                                    [1, 2, 4, 1], [1, 3]),
                description.Journey(3, utils.calculate_journey_length([1, 3, 1], self.problem_desc),
                                    [1, 3, 1], [2]),
            ]
        )

        sol4 = description.Solution(
            [
                description.Journey(7, utils.calculate_journey_length([1, 2, 3, 4, 1], self.problem_desc),
                                    [1, 2, 3, 4, 1], [1, 2, 3]),
            ]
        )

        sol5 = description.Solution(
            [
                description.Journey(7, utils.calculate_journey_length([1, 2, 4, 3, 1], self.problem_desc),
                                    [1, 2, 4, 3, 1], [1, 2, 3]),
            ]
        )

        self.assertEqual(hillclimb_check(sol1, sol2, self.problem_desc), sol2, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol2, sol3, self.problem_desc), sol3, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol3, sol4, self.problem_desc), sol4, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol4, sol5, self.problem_desc), sol5, "Longer solution should not be cheaper")

        self.assertEqual(hillclimb_check(sol3, sol1, self.problem_desc), sol3, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol4, sol2, self.problem_desc), sol4, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol5, sol3, self.problem_desc), sol5, "Longer solution should not be cheaper")
        self.assertEqual(hillclimb_check(sol5, sol1, self.problem_desc), sol5, "Longer solution should not be cheaper")


if __name__ == '__main__':
    unittest.main()
