import unittest

from test_folder.utils_tests import *
from test_folder.generator_tests import *
from test_folder.optimizer_tests import *

if __name__ == '__main__':
    unittest.main()
