\contentsline {chapter}{Introduction}{3}{chapter*.2}
\contentsline {section}{Goals}{3}{section*.3}
\contentsline {chapter}{\numberline {1}Installation guide}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Prerequisites}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Installation}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Getting problem data}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}User guide}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Main runner}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Compulsory argument}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Optional arguments}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Examples}{6}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Benchmarking and data generation}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Unit tests}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}Makefile}{7}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Examples}{7}{subsection.2.4.1}
\contentsline {chapter}{\numberline {3}Developer documentation}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Overall paradigms}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Problem specific data structures}{9}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Machine}{9}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Location}{9}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Request}{9}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Journey}{9}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Solution}{10}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}problem\_dict}{10}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}Utility functions}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Validation functions}{10}{subsection.3.3.1}
\contentsline {subsubsection}{validate\_journey}{10}{section*.9}
\contentsline {subsubsection}{check\_capacity}{10}{section*.10}
\contentsline {subsubsection}{check\_journey\_length}{11}{section*.13}
\contentsline {subsubsection}{calculate\_journey\_length}{11}{section*.14}
\contentsline {subsection}{\numberline {3.3.2}Path manipulation}{11}{subsection.3.3.2}
\contentsline {subsubsection}{add\_location\_to\_path}{11}{section*.15}
\contentsline {subsubsection}{remove\_location\_from\_path}{11}{section*.16}
\contentsline {subsubsection}{get\_location\_from\_path}{11}{section*.17}
\contentsline {subsection}{\numberline {3.3.3}Misc}{11}{subsection.3.3.3}
\contentsline {subsubsection}{get\_id}{12}{section*.18}
\contentsline {section}{\numberline {3.4}Verolog Challenge instance parser}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Solution construction}{12}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Base construction}{12}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Giant tour construction}{12}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Weighted giant tour construction}{12}{subsection.3.5.3}
\contentsline {section}{\numberline {3.6}Solution optimization}{13}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}switch\_requests}{13}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}fragment\_journey}{13}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Local search cutoff}{13}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}optimize}{13}{subsection.3.6.4}
\contentsline {section}{\numberline {3.7}Solution validation}{14}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}validate\_solution}{14}{subsection.3.7.1}
\contentsline {section}{\numberline {3.8}Unit tests}{14}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}optimizer\_tests}{14}{subsection.3.8.1}
\contentsline {subsubsection}{request\_moving}{14}{section*.19}
\contentsline {subsubsection}{request\_removal}{14}{section*.20}
\contentsline {subsubsection}{request\_switching}{14}{section*.21}
\contentsline {subsubsection}{solution\_comparison}{14}{section*.22}
\contentsline {chapter}{\numberline {4}Summary}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Achieved goals}{15}{section.4.1}
\contentsline {chapter}{Bibliography}{16}{chapter*.23}
