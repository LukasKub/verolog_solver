\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
In recent years the growth of e-commerce has led to greater emphasis on last mile deliveries and the efficient routing of these deliveries to provide the best service to costumers. 
This has led to more research contributions in this space, particularly picking up in the last few years \cite{e-commerce}.
Bringing these advances together is, among others, the Vehicle Routing and Logistics Optimization Working Group (VeRoLog) Solver Challenge \cite{verolog info}, a yearly competition where teams compete to solve real world problems dealing with vehicle routing.

Among the most interesting approaches of the past few years we can find meta-heuristical evolutionary algorithms \cite{vrp tw survey}.
%The basis is a simple evolutionary algorithm \cite{basic evolution}, where the main emphasis is on the mutation stage.
The main idea behind these is to move the brunt of the work from the programmer to the algorithm.
Instead of having to come up with complicated heuristics, which have to deal with all the different potential edge cases which can arise, simple heuristics are implemented instead.
Furthermore instead of having to integrate all of these heuristic manually, another heuristical layer that utilizes these lower level heuristics is implemented instead.

This can provide a much more adaptable algorithm, which can learn from the data set in question and use heuristics that are better suited to the current data.
Also this approach is much easier to extend with new heuristics, as when all of these heuristics use the same interface, any new heuristic with the same interface is simply a drop-in replacement.

In this thesis we inspect several of these low-level heuristics, implement them in a meta-heuristical algorithm solving a simplified version of the VeRoLog Challenge 2019 and compare them based on their performance.

\newpage
%\section{Problem Description}
\section*{Problem Description}
\addcontentsline{toc}{section}{Problem Description}

Let us begin by first providing a description of the problem considered in this thesis.
The problem selected is the VeRoLog Solver Challenge 2019 (VeRoLog Challenge) \cite{verolog info}, which is an open problem with publicly available description and instance data.

%\subsection{Original Problem Description}
\subsection*{Original Problem Description}
\addcontentsline{toc}{subsection}{Original Problem Description}

The original VeRoLog Challenge problem description can be found at the link \cite{verolog info}, here we provide only a short summary.
In short the VeRoLog Challenge deals with the delivery of machines from a central depot to secondary locations and the installation of these machines by technicians

\begin{itemize}
\item There exists a central depot with machines.

\item There exist a number of requests for machines which have to be fulfilled.

\item Each request denotes which machines need to be delivered, to which location and the time window for the delivery.

\item Each delivery is made by a truck, the number of which is not limited. All trucks are based at the depot and have a maximum capacity and maximum distance they can cover in a day. Each truck must load the machines at the depot and unload machines at the respective delivery locations. A single truck can load machines multiple times. Every truck must return to the depot at the end of the day.

\item After delivery, all machines must be installed by a technician. The installation cannot be done in the same day as the delivery.

\item Technicians are based at secondary locations, have a maximum distance they can travel each day, have a certain set of machines they can install and are limited in number. They also must return to their starting location at the end of a day.

\item The goal of the problem is to fulfill all requests by delivering and installing all the requested machines, while using the least amount of resources - trucks, technicians and sum of distances traveled by both.
\end{itemize}

%\subsection{Problem Simplification}
\subsection*{Problem Simplification}
\addcontentsline{toc}{subsection}{Problem Simplification}

As this problem is a complex real-world problem and the goals of this thesis are to compare low level heuristics, we decided to reduce the scope of the problem by removing those conditions which increase complexity, but have to be solved independently of the Vehicle Routing Problem (VRP) itself.

The most obvious extra parameter is machine installation by the technicians.
This parameter forces any solution to the overall VeRoLog Challenge to solve a second routing problem and is further compounded by the fact that technicians are based at secondary locations, different from the depot, which moves the problem into another category entirely.
Thus, we decided to focus only on machine delivery and to disregard installation entirely.

The second removed parameter is the time window constraint for deliveries.
Again the time window constraints complicate the problem, but have to be solved by more complicated heuristics and approaches.
Removing this parameter has allowed the work to focus on the routing aspect of the VeRoLog Challenge.

%\subsection{Simplified Problem Description}
\subsection*{Simplified Problem Description}
\label{simplified problem description}
\addcontentsline{toc}{subsection}{Simplified Problem Description}

The simplified problem is the delivery of requests from a central depot to secondary locations using delivery vehicles.
Requests are transported by delivery vehicles, each request has to be filled by a single delivery, but a single vehicle can deliver multiple requests - either more requests to the same location or different requests to different locations.
Vehicles are limited in capacity and range and have to return to the depot at the end of their route.

\subsection*{Instance Data}
The following data is given by the instance data and is immutable and cannot be modified in any way.
\paragraph{Locations}
There is a depot $ D $ and $ m $ delivery locations $ L = \{ l_{1}, l_{2}, ... , l_{m} \}$ with distinct coordinates on a plane.
The distance between locations is the Euclidean metric of their respective coordinates.
\paragraph{Requests}
There are $ n $ requests $ R = \{r_{1}, r_{2}, ..., r_{n}\}$.
Each request $ r \in R $ is tied a single location $l(r)$, where the delivery must be completed.
More than one request can require delivery to the same location, but each request is assigned a single location.
Each request $ r \in R $ has a certain weight $w(r)$, which determines how much of a vehicle's capacity is needed to transport given request.
Requests cannot be split; i.e. the entire request must be delivered by one single delivery vehicle.
\paragraph{Delivery vehicles}
Delivery vehicles deliver requests from the depot to the locations.
All delivery vehicles have the same capacity and maximum distance of travel, which we label $CAPACITY$ and $RANGE$ respectively.
The number of delivery vehicles is unlimited.
\paragraph{Score}
Each solution of the problem has a score, which has two components.
There is a fixed cost for every delivery vehicle, and there is a cost for every unit of distance traveled.
As the two components of a solution are costs, the quality of a solution increases with a decrease of the score value.
\paragraph{Goal}
The goal is to find a solution, with the minimum possible score.


\subsection*{Solution Characteristics}
The following data structures are not part of the instance data.
Instead, these data structures form the solution.
\paragraph{Journeys}
Each journey $ j $ represents the trip of one delivery vehicle, starting at the depot, delivering certain requests $requests(j) \subseteq R$ and following a path $path(j)$ of locations and ending back at the depot.
Let $length(j)$ be the length of $ path(j) $, and $weight(j)$ be the sum of all the weights of the requests this journey delivers; i.e. $ w(j) = \sum_{ r \in requests(j)} w(r) $.
A journey's maximum capacity is the maximum capacity of a delivery vehicle and so is the maximum range.
\paragraph{Solutions}
A solution $ S_{i} $ is a set of journeys.
A feasible solution is a solution, which satisfies all the problem requirements: it delivers all the requests and the weight and length constraints are obeyed.
From this point any solution mentioned is, unless otherwise specified, thought to be feasible.

%\section{State of the Art}
\section*{State-of-the-Art}
\addcontentsline{toc}{section}{State-of-the-Art}

In general the Vehicle Routing Problem (VRP) is a well-studied problem \cite{review of VRP}, and in this section we detail the advances in research that helped shape the direction and methods of this thesis.

In the work of Wu et al. \cite{neighbor search VRP}, a full heuristic algorithm for the Split Delivery Vehicle Routing Problem (SDVRP) \cite{SDVRP survey} is detailed, based on an adaptive large neighborhood search framework.
Their work is based on the possibility of splitting and combining costumers into costumer-commodities, where either the costumer is regarded as one entity with all commodities delivered at once, or as several entities, where each commodity can be delivered separately.

Zhang et al. \cite{VRP metaheuristics} presented two different approaches to solve the VeRoLog Solver Challenge 2016–2017.
The first approach is a sequence-based hyper-heuristical approach, which combined many low level heuristics to produce a much stronger meta-heuristic.
The second approach uses a two stage algorithm to solve first the time constrains and then the routing itself. 

The common ideas in both of these are the usage of low level heuristics in multi-level algorithms and operations on the level of journeys and requests.

%\section{Heuristics of State-of-the-Art Algorithms}
\section*{Heuristics of State-of-the-Art Algorithms}
\addcontentsline{toc}{section}{Heuristics of State-of-the-Art Algorithms}

As we want to compare different heuristics for solving VRP, here we detail several of those that were mentioned in the previously mentioned papers, i.e. \cite{neighbor search VRP, VRP metaheuristics}.
The most interesting heuristic are the ones dealing with assigning requests to journeys, as that is the focus of this thesis.
As a side note both \cite{neighbor search VRP, VRP metaheuristics} provide several heuristics dealing with routing within a particular journey.
As routing can be by brute force for the data sets used in this thesis (see details in Section \ref{Journey Routing}), these heuristics are not considered here.

Both \cite{neighbor search VRP, VRP metaheuristics} provide a Move requests between journeys and a Swap request pair heuristic, but \cite{VRP metaheuristics} also notes a Move block of requests and Swap blocks of requests heuristic, while \cite{neighbor search VRP} details Move costumer and Swap costumer heuristics and, for this thesis most importantly, the Shaw removal heuristics.
All of these heuristics are detailed in Section \ref{heuristic overview}, with the Shaw removal heuristic further expanded in Section \ref{shaw removal}.

\iffalse
\subsection*{State-of-the-Art Heuristics}

\begin{itemize}
\item Move between routes: move one request from one journey to another.
\item Swap between routes: swaps a pair of requests between two journeys.
\item Move block between routes: moves a sequence of requests from one journey to another.
\item Swap block between routes: swaps sequences of requests between two journeys.
\item Ruin and recreate: picks a journey and recombines the requests to provide several viable journeys.
\item Move costumer: takes all the requests which correspond to a single location and moves them to a different journey.
\item Swap costumer: swaps all the requests from a single location in one journey with all the requests from a single location from another journey.
\item Shaw removal: removes a number of requests chosen in a particular way, which are then randomly reinserted. This method is detailed in section \ref{Shaw Removal}
\end{itemize}

Most of these heuristics are simple and straightforward, with the idea of moving or swapping a single request or more from one journey to another.
The most interesting is Shaw removal and the ideas behind it.

\fi

%\section{Contribution of the Thesis}
\section*{Contribution of the Thesis}
\addcontentsline{toc}{section}{Contributions of the Thesis}

In this section we detail the contributions which we bring in this thesis. 
We first detail a low level heuristic and then the way in which the algorithms, detailed in Section \ref{algorithm description}, were implemented.
%\subsection{Shaw Heuristic}
\subsection*{Shaw Heuristic}
\addcontentsline{toc}{subsection}{Shaw Heuristic}

The main theoretical contribution that we bring in this thesis is the Shaw Heuristic.
The Shaw Heuristics is a low level heuristic, which deals with choosing requests for higher level heuristics.
For example, where the Swap Requests heuristic would normally choose these requests randomly, the Shaw Heuristics is able to make a better choice based on the properties of the instance in question.
%\subsection{Framework Implementation}
\subsection*{Framework Implementation}
\addcontentsline{toc}{subsection}{Framework Implementation}

All the algorithms detailed in Section \ref{algorithm description} have been implemented in Python 3.7 and available either as an attachment to this thesis, or on gitlab\footnote{\href {https://gitlab.com/LukasKub/verolog\_solver}{https://gitlab.com/LukasKub/verolog\_solver}}.
This framework includes both the implementation of the algorithms and a framework for benchmarking and running the experiments, as detailed in Section \ref{experiments}.
%\subsection{Framework Modularity}
\subsection*{Framework Modularity}
\addcontentsline{toc}{subsection}{Framework Modularity}

Furthermore, this framework has been designed to be highly modular, with several levels of heuristics, each of which have a common interface.
This allows for easy drop in replacements and expansions of the existing algorithms and can be used to compare new heuristics against the ones already implemented.

This Framework is described in detail in the Developer Documentation in Appendix \ref{developer documentation}.
%\section{Thesis Overview}
\section*{Thesis Overview}
\addcontentsline{toc}{section}{Thesis Overview}
The thesis itself is organized in the following way.

Chapter \ref{theory} contains a theoretical description of the heuristics used in State-of-the-Art algorithms and the description of the heuristics used in this thesis.
In this Chapter we also present the theoretical contribution, the Shaw Heuristic.

In Chapter \ref{algorithm description} we describe how these heuristics are brought together to form the overall algorithms from a theoretical point of view, with more details in Appendix \ref{developer documentation}.

In Chapter \ref{experiments} the experiments comparing the heuristics and algorithms are described and the results presented and discussed.

We provide an Installation Guide in Appendix \ref{installation guide}, which deals with installation of the software used in this thesis.

A User Guide is detailed in Appendix \ref{user guide} which provides an overview of the usage of the software provided.

Lastly we provide the Developer Documentaiton in Appendix \ref{developer documentation} which details the implementation itself and how this implementation works on a practical level.
