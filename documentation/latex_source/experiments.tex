\chapter{Experiments\label{experiments}}
To compare different approaches to solving the VRP, several experiments were devised and carried out.
These experiments were designed to be reproducible, representative and comparable.

\section{Environment Overview}
We first note the environment in which the experiments were carried out.

On the hardware side, all experiments were run on a desktop 8 core, 16 thread AMD Ryzen 7 3700X processor with 16GB of DDR4 3200MHz CL16 memory.
On the software side, a Ubuntu 20.04 LTS distribution was used, with the native Python 3.8.5 interpreter.
All the Python libraries used were the latest stable versions as of April 2021.
\section{Experiment 1: Generation}
\label{generation experiment}
In Experiment 1 we compare the Trivial Generator, the Random Tour Generator and the Shaw Tour Generator, as detailed in Section \ref{solution generation}, which are used to generate solutions.

\subsection*{Methodology}
Experiment 1 was run on the ORTEC-early\footnote{\href {https://verolog2019.ortec.com/instance/5/ORTEC-small.zip}{https://verolog2019.ortec.com/instance/5/ORTEC-small.zip}} data set of instances.
The instances in this data set have between 100 and 1000 requests and between 50 and 500 locations.

As both the Random and Shaw Tour Generators are based on randomization they have been run multiple times and the data then interpreted.
Both the Generators were run 100 times to provide the data in Figure 3.1, but the results were consistent even when increasing the number of runs to 1000.

\subsection*{Results}
The data shown in Figure \ref{generation figure} is the relative score of the Random Tour Generator and Shaw Tour Generator to the Trivial Solution Generator for 100 runs of the generators on the ORTEC-early data set.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{generation_graph_ORTEC_early.png}
\caption{Generator Benchmarks on the ORTEC-early instance data set\label{generation figure}}
\end{figure}

From the results we can see that the Random Tour Generator is a better approach then the Trivial Generator, generating on average a solution which is 15 percent better.
But the Shaw Tour Generator is better in every instance, generating on average solutions 30 percent better than the Trivial Generator and 10 percent better than the Random Tour Generator.

From the dispersion we can see that these results are consistent, with minimal overlap, which is to be expected.
When running this experiment on a much smaller instance (for example the ORTEC-small data sets) a much larger overlap was detected, as the Random Heuristic, used by the Random Tour Generator, much more often generated the same list as the Shaw Heuristic, used by the Shaw Tour Generator, which resulted in the generators generating the same solutions more often.

\section{Experiment 2: Optimization}
In Experiment 2 we compare different optimization heuristics which are used to optimize solutions.

\begin{itemize}
\item \textbf{Random Hillclimb Algorithm}.
The hillclimb metaheuristic is used with only randomized low level heuristics.
\item \textbf{Shaw Hillclimb Algorithm}.
The hillclimb metaheuristic is used with both the random and the shaw low level heuristics.
\item \textbf{Random Annealing Algorithm}.
The annealing metaheuristic is used with only randomized low level heuristics.
\item \textbf{Shaw Annealing Algorithm}.
The annealing metaheuristic is used with both ther random and the shaw low level heuristics.
\end{itemize}

\subsection*{Methodology}
Experiment 2 was run on the ORTEC-late´\footnote{\href {https://verolog2019.ortec.com/instance/3/ORTEC-late.zip}{https://verolog2019.ortec.com/instance/3/ORTEC-late.zip}} data set of instances.
The instances in this data set have between 100 and 1200 requests and between 30 and 300 locations.

All algorithms were run for 50 000 generations a total of 8 times to provide the results.
The initial solution was the same for all the algorithms in all runs and it was the best solution found from the Shaw Tour Generator when run a total of 100 times for the given instance.
These settings were chosen to showcase the differences between the different algorithms, which truly come into effect when the solutions are nearing the optimum.

An interesting point to note is the difference in the actual time that the different algorithms took to complete these 50000 iterations and where the main slowdown was.
Both the Random algorithms took a similar amount of time to compute - just under a minute for 50 000 iterations - but the Shaw algorithms twice as long - around a minute and a half for 50 000 iterations. 


The main slowdown in all of these algorithms was the solution score calculation, which has to be run in every iteration for every candidate.
The difference in times between Random and Shaw algorithms is due to the heuristic having to find the similar requests.
Generating a list of requests based on similarity can be precomputed beforehand, but the problem is finding these requests in the solution data structure. 
And while not computationally expensive on its own, this has to be done multiple times in each iteration.

\subsection*{Results}
The full run of all the algorithms on one instance is detailed in Figure \ref{single optimization figure}.
Only the average score of all the solutions attained in each generation is noted for each algorithm.
While there are instances, on which the random algorithms surpass the Shaw algorithms, Figure \ref{single optimization figure} is representative for the average Experiment run.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{ORTEC_Late_11.png}
\caption{Optimization benchmarks on the ORTEC-late 11 instance\label{single optimization figure}}
\end{figure}

Firstly let us compare the Hillclimb and Annealing metaheuristics.
We can see that when used with the Shaw heuristic, the Annealing metaheuristic can lead to better results, even though the improvement is not major.
On the other hand, combining randomized heuristics and Annealing does not lead to any improvement, instead producing slightly worse results on average.

The comparison of Shaw and Random Heuristics is more straightforward.
In the beginning these two heuristics are comparable and the difference only comes into play once the solution nears an optima and progress slows.
But then the difference becomes clear, with the Shaw heuristic coming out ahead.
This is again confirmed by the data for the other instances, detailed in Table \ref{result table}.

Figure \ref{optimization graph} summarizes the data of Table \ref{result table}.
In the Figure the values have been normalized so that results for different instances can be compared.
The Base Optimization Score is the score of the solution generated by the Shaw Tour Generator, on which the optimization was started, while the Trivial Score is the score of a trivial solution for a given instance.

From these results we can see that in several instances the Shaw Heuristic does perform better than the Random heuristic, but there are cases, when the difference is marginal or even in the Random heuristic's favor.
On the other hand, as in the specific instance optimization run, the difference between Hillclimb and Annealing metaheuristics is negligible.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{optimization_graph_late.png}
\caption{Optimization benchmarks on all ORTEC-late instances\label{optimization graph}}
\end{figure}

\newpage
\begin{sidewaystable}
\centering
%\begin{tabular}{|p{1.8cm}|*{6}{|p{3.1cm}}| }
\begin{tabular}{|p{1.8cm}|*{6}{|p{3.1cm}<{\raggedleft}}|}
 \hline
 \multicolumn{7}{|c|}{Optimization benchmark results} \\
 \hline
 Instance name & Trivial solution score & Shaw Generation score & Random Hillclimb score & Shaw Hillclimb score & Random Annealing score & Shaw Annealing score \\
 \hline
\input{optimization_results}
 \hline
\end{tabular}
\caption{Optimization benchmark results\label{result table}}
\end{sidewaystable}
