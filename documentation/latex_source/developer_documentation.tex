\chapter{Developer documentation}
\label{developer documentation}
In this section details concerning the development of the software are detailed.
%\section{Overall paradigms}
%Functions are good.
\section{Problem Specific Data Structures}
In this section all the problem specific data structures are detailed.
Most of these are just the VeRoLog Challenge data types, as detailed in the \hyperref[simplified problem description]{Simplified Problem Description} in the Introduction, converted into a data structure, but some have been extended with additional methods and/or values.
All data structures also have a \textit{str} method, which formats the data they keep and allows for better debugging.

It has been found to be much better to keep data in special data structures instead of tuples of values, so even a simple thing such as a location, which only has coordinates and an ID, has a special data type, as it allows the code to be much more readable and understandable.
\subsection{Location Class}
Locations are the representation of locations as in the problem description.
They have an ID, \textbf{x} coordinate and \textbf{y} coordinate.
\subsection{Request Class}
Requests have a request ID, a location ID and a weight.
\subsection{Journey Class}
A journey has a weight, length, path and request list.
The path is a list of locations, which the journey goes through, beginning with the depot and ending at the depot.
\subsection{Solution Class}
The solution is a list of journeys, which is all that a solution really is.
It also provides a \textit{get\_requests} method, which returns all the request satisfied by this solution.
This method is most useful for debugging purpuses, as the algorithm is designed to work on feasible solutions only.
\subsection{Problem Description Class}
\label{problem description class}
The problem description class is keeps the entire instance in a single place with an easy interface.
All the locations, requests and other invariants, such as the \textit{CAPACITY} or \textit{RANGE} are kept in this data class and available through member variables.
Most importantly, the problem description does not contain any journeys or solutions.

\section{Algorithm Implementation}
In this section we detail how the algorithm is implemented and how we achieved modularity and extendability and the lower level functions are described in detail in subsequent sections.

\subsection{Optimization Algorithm Classes}
The aforementioned local search heuristics and algorithms are implemented in a modular structure, as described in Section \ref{algorithm description} and all the code which enables this is kept in the \textit{optimization} folder.

The common interface is that all of these classes have single ancestor class with a \textit{problem\_desc} member variable, which stores the problem description data class and the \textit{optimize} method with the following notation

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=4, keywordstyle=\color{magenta}]
class OptimizeBaseClass:
    self.problem_desc = problem_description
    def optimize(source_solution: Solution) -> Solution:
        return source_solution
\end{lstlisting}

And all the other classes are descended from this class and just implement the \textit{optimize} method differently.

We can see that the problem description data class is not passed here, so it has to be kept by the classes themselves.
And more importantly any class with the same optimize method with the same notation can be dropped in as a replacement to any other class in this hierarchy.
\subsubsection{Local Optimization Classes}
The Local Optimization Classes are kept in the \textit{local\_optimization.py} file, implement a single heuristic (request movement or swapping) and provide the common interface.
The common ancestor of the Local Optimization Classes is a LocalOptimizationClass, which stores the local search function (request movement or random/shaw swapping) and implements a large \textit{optimize} method, so that all the descendants just specify the local search function and the \textit{optimize} method is the same for all of them.
This helps modularity and expandability, as any new local search heuristic only needs to implement the local search method and all the logic is contained in the base class.

This method has two modes, with one checking the validity of all the modifications to the solutions and logging all that happens with the given solution and the other one just running the function and presuming everything is ok.

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=2, keywordstyle=\color{magenta}]
class LocalOptimizationClass(OptimizeBaseClass):
	self.local_search_function: local_search_fuction
	self.mode = "DEBUG"/"RELEASE"
    
	def optimize(source_solution: Solution) -> Solution:
		if self.mode == "DEBUG":
			logger.debug("Running function {}".format(
				self.local_search_function)
					)
			new_solution = self.local_search_function(
				solution, self.problem_desc
					)
			validator.check_validity(
				new_solution, self.problem_desc
					)
            
			except RuntimeError as e:
				logger.error(e)
				raise
            
			return new_solution

		else:
			return self.local_search_function(
				solution, self.problem_desc
					)
            
\end{lstlisting}


All the local search heuristics have their own descendant classes, which just specify the \textit{local\_search\_function}.
\subsubsection{Local Search Classes}
Local search classes are a little more complex and are kept in the \textit{local\_search.py} file.
They implement either Hillclimbing or Simulated Annealing, and so must keep a list of all the Local Optimization Classes, which they use during the neighborhood search phase.
These classes also have to know how deep and wide the local search is supposed to be, and they also need to keep a copy of the problem description.

Both the local search heuristics mentioned are implemented as specialised classes of a common ancestor class, the LocalSearchClass.
And again all the logic (how many candidates and how deep the neighborhood exploration is to be) is done in the base class and the two descendants only provide a boundary class or method and use the implemented logic, which again eases the expandability.

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=2, keywordstyle=\color{magenta}]
class LocalSearchClass(OptimizeBaseClass):
   	self.local_opt_class_list: [LocalOptimizeClass]
	self.depth: int
	self.width: int
	self.boundary_function = hillclimb/annealing function
    
	def optimize(source_solution: Solution) -> Solution:
   		candidates: [Solution] = []
		for _ in range(self.width):
	    	candidate = source_solution
	    	for_ in range(self.depth)
            	for local_opt_class in 
                	self.local_opt_class_list:
            	candidate=local_opt_class.optimize(candidate)
            	candidates.append(candidate)
    
    	candidate_solution = min(candidates,
        	key=itm: validator.get_score(
            	itm, self.problem_desc)
                	)
        
    	next_solution = self.boundary_function(
        	candidate_solution, source_solution)
        
    	return next_solution
\end{lstlisting}


\subsubsection{Algorithm Classes}
Algorithm Classes, contained in the \textit{algorithms.py} file, are a step above the Local Search Classes and as such keep a Local Search Class to use for the optimization part itself, while keeping track of the iteration number and the scores achieved in each iteration of the algorithm and also provide a method for getting this data after the optimization has finished.

Again there is an AlgorithmBaseClass, which provides all the functionality, while the algorithms themselves are just specializations of this class.

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=2, keywordstyle=\color{magenta}]
class AlgorithmBaseClass(OptimizeBaseClass):
	self.local_search_class: LocalSearchClass
	self.iterations: int
	self.log_interval: int
	self.scores = [int]
    
	def optimize(source_solution: Solution) -> Solution:
		for i in range(self.iterations):
			solution = self.local_search_class.optimize(solution)
			score = validator.get_score(solution)
			self.scores.append(score)
        
			if i % self.log_interval == 0:
				logger.info("Iteration {} score {}".format(
					i, score))
        
		return solution
\end{lstlisting}

All the local search heuristics are contained in the LocalSearchClass, as the overall algorithm does not need to know about them to do what it needs to do.

\section{Problem Specific Functions}
The rest of the code is specific to the given problem and is kept in the \textit{source} folder.
\subsection{Parsing}
In this part, the code dealing with parsing a file containing a single VeRoLog Challenge instance is detailed.

The only function in the \textit{instance\_parser.py} file is the \textit{parse\_file} function, which does everything - reads the given file, properly parses the data, creates the data structures and prepares a single \textit{problem\_description} data structure for keeping the instance data.
\subsection{Validation}
In the \textit{validator.py} file, the functions which deal with validating solutions and scoring solutions are kept.

The first function is the \textit{validate\_solution} function, which checks all journeys for capacity, length and path, using the utility functions, and then checks the solution for proper request fulfillment.
This function can be run verbally, where any problem results in a RuntimeError and a trace to enable better debugging.

The second function is the \textit{get\_solution\_score} function, which does not check anything, but instead presumes the solution is valid and calculates its score.
\subsection{Generation}
In the \textit{generator.py} the different generators, as detailed in Section \ref{solution generation}, are implemented.

The first generator, the Trivial Generator, is implemented in a straightforward manner, while the other two Giant Tour generators differ only in the way the permutation of requests is generated, and as such the common code is kept in a helper function.
This also enables a new generator, which would use another heuristic to generate the request permutation, to be implemented trivially.
The Random Tour Generator uses a simple shuffle function, while the Shaw Tour Generator uses the Shaw Heuristic, which is implemented in the Local Search Functions.

\subsection{Local Search Functions}
In the \textit{local\_search} folder the local search heuristics are kept.
By this we mean the underlying code, which is specific to the given heuristic, not the modular classes, which make up the final algorithm.
\subsubsection{Local Search Algorithms}
Functions and classes, which deal with the local search algorithms are provided in the \textit{local\_search\_algorithms.py} file.
As the hillclimb check is a simple comparison, it is simply function, but simulated annealing has to remember the temperature, so it is defined as a callable class, with the same interface as the hillclimb check function.
\subsubsection{Helper Functions}
Functions for removing and adding requests to journeys are provided in the \textit{helper\_functions.py} file.
These functions check the validity of the new journeys (capacity and range) and return a fail code, if the addition/removal fails.
The reason for returning a code and not throwing an error or returning the original data structures is to reduce overhead as this manipulation fails to produce valid journeys often.
\subsubsection{Request Movement}
Two functions are implemented in the \textit{request\_movement.py} file.
The first is the actual movement of a request, which actually removes one request from one journey and adds it into another journey, using the two helper functions.
The \textit{move\_request} function actually checks weather the movement (both the removal and the addition of a request to the respective journeys) was successful and either returns the new solution or the original solution.

The second function is a random move function, where a request is randomly selected to be moved.
\subsubsection{Request Swapping}
Similarly to request movement, the functions in the \textit{request\_swapping.py} file deal with all the functions used to swap requests.
First there is a function, which actually swaps requests, again using the journey manipulation functions, just checking them for success and returning the valid solution.

Then there are two functions, which actually swap requests.
Firstly a random swap function, which swaps two random requests, and a shaw swap function, which swaps two requests which are generated by the shaw heuristic.
\subsubsection{Shaw Simmilarity}
The two Shaw Heuristic functions, as described in Section \ref{shaw heuristic} are implemented in the \textit{shaw\_simmilarity.py} file in practically the same as those in the example given, the only difference is that in reality the results are not precomputed, but cached throughout the run.
Caching is done through a dictionary instead of using an in-build caching function, as hashing cannot be done for the problem description data structure and all the shaw similarity functions need this data structure to work correctly.

\section{Utilities}
The first main section of code are the utility functions, kept in the \textit{utils} folder, which provide certain general ways to manipulate the given data.
\subsection{Logging}
A small logger library is provided in the \textit{logging.py} file based on the standard Python \textit{logging} library.
All logs have their level and timestamp prefixed and are either logged to a file or to standard output.
The levels supported are \textit{debug, info, warning} and \textit{error}.

\subsection{Utility Functions}
Several utility functions are provided in the \textit{utils.py} file.
The most important part of these functions is that all outfacing functions require a problem description class as detailed in Appendix \ref{problem description class}.
\subsubsection{Validation Specific Functions}
There are individual functions for checking tha capacity and length constraints of a particular journey and also a function for checking the path of a journey. 
Capacity and length checks are obvious, the path check is simply whether the journey passes through all the locations of its requests.
This might seem like a trivial check, but it is a good way to check the validity of request manipulation functions and has uncovered several errors.
\subsubsection{Path Manipulation Functions}
There exist several functions, which manipulate the paths.
The most important is \textit{add\_location\_to\_path} and \textit{remove\_location\_from\_path}, which not only add/remove locations, but also return the shortest path through the new locations and also properly take care of starting and ending the path in the depot.
This includes generating all the permutations of the locations and calculating the journey lengths.
\subsubsection{Miscellaneous Functions}
A function for generating a random pair of indexes is here, as well as a legacy function for getting IDs from data, which might be a data structure or already an ID.
