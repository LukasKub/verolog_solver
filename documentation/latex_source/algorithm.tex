\chapter{Algorithm description \label{algorithm description}}

\section{Overview}

The process of solving the Reduced VeRoLog Solver Challenge can be split into two parts - attaining a feasible solution, discussed in Section \ref{solution generation}, and optimization itself, discussed in Section \ref{optimization}.
The heuristics used in the optimization are also of interest, as these have a large impact on the final result.

\section{Solution Generation}
\label{solution generation}
First we consider solution generation, since a feasible solution is required for all the subsequent steps.
Three types of generation were considered - one trivial, one greedy and one heuristical method.

\subsection{Trivial Generator}
A solution is called trivial if every journey fulfills exactly one request.
Note that for each instance there exists one trivial solution (up to the reordering of journeys).
The Trivial Generation creates a trivial solution.

\paragraph{Observation}
%%\begin{theorem}
\textit{The instance has a feasible solution if and only if the trivial solution is feasible.}
%%\end{theorem}
\paragraph{Proof}
$<=$
If the trivial solution is feasible, then a feasible solution to the instance exists.

$=>$
We shall show that from a feasible solution the trivial solution can be constructed, without breaking any of the feasibility requirements.

Let us consider a feasible solution with a journey $ j $ that delivers more than one request.
We split this journey into several journeys, all of which deliver only one of the requests of the original journey.
Due to the triangle inequality the length of the path cannot increase when removing locations from the path, thus the distance requirement must be satisfied for all the journeys.
Because all weights are positive numbers, the weight of any request from a set of requests must be smaller than the sum of all the requests of said set.
We continue splitting journeys until no more can be split, at which point we arrive at the trivial solution for a given instance $\square$.

In the rest of this thesis we shall only consider feasible instances.

\subsection{Giant Tour Generation}
In the second type of generation, a permutation of requests is made and then journeys are created based on this permutation. 
Each time we start with an empty journey and keep adding requests until adding the next one would make a journey that would break the $CAPACITY$ or $RANGE$ conditions. 
We add this journey to the solution, take an empty journey and begin with the next request.

In this thesis we have two different Giant Tour Generators, which differ in the way the permutation of requests is generated.

\begin{itemize}
\item 
\textbf{Random Tour Generator}
The first Giant Tour Generation uses a random permutation of requests, upon which it generates the giant tour.
\item
\textbf{Shaw Tour Generator}
\label{shaw tour}
The other Giant Tour Generation uses the Shaw Heuristic, as detailed in Section \ref{shaw heuristic}, to generate a permutation of requests, upon which the giant tour is generated.
\end{itemize}

We compare these three generators in Section \ref{generation experiment}.

\section{Optimization\label{optimization}}
After obtaining a feasible solution, optimization can begin. 

\subsection{Optimization Algorithm}
The basic optimization algorithms structure is based on the large neighborhood search as detailed in \cite{neighbor search VRP} and also draws some inspiration from the base algorithm detailed in \cite{basic evolution}.

The algorithm starts with a feasible solution.
In each iteration several candidate solutions are generated using several low-level heuristics.
The best of these candidates is chosen and compared to the source solution for a given iteration, to provide the source solution for the next iteration.

This process is repeated until a constraint, such as time, number of iterations or score improvement speed, is reached.

\subsection{Journey Routing}
\label{Journey Routing}
The heuristics used in this thesis deal with journey manipulation and so it is important to consider routing within a given journey; i.e. in which order are the requests of a given journey delivered - in which order are their locations visited.
In the data sets of this VeRoLog Challenge the parameter $CAPACITY$ is always lower than $20$ and the request weights are in the range of $3-12$, which means that at most six requests can be delivered by a single journey.
Thus, we decided not to route using smart algorithms but to route by brute force instead.

This was later revealed to be the right approach, as the performance impact on the overall algorithm is marginal and more computation time can be spent elsewhere.

\subsection{Local Search Heuristics}
There are several local search heuristics, which are implemented in this work, all of which are based around Request manipulation. 

\subsubsection{Request Movement}
Two journeys are chosen and a request is then moved from one journey to the other.
While this approach seems very natural, it runs into some problems - when all journey weights are near or at capacity, this function has practically no chance of improving the solution.

\subsubsection{Request Swapping}
A better approach is to swap requests instead of just moving them.
Two requests are chosen and then swapped between their respective journeys.
The way in which these two requests are chosen can either be random or similarity based, which we detail in Section \ref{request selection}.

%\subsubsection{Request removal}
%The previous two approaches logically tend to improve solutions, but can lead to local optima.
%Thus another local search function generate a worse solution to leave the local optima.
%A number of requests is chosen and they are removed from the journeys they currently belong to and are satisfied by new journeys.
%
\subsection{Local Search Algorithms}
After neighborhood exploration a candidate solution is presented and compared to the base solution.
Two method are used to choose the solution which will form the base for the next iteration.
\subsubsection{Hillclimbing}
A simple greedy approach, which always chooses the better solution.
\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=2, keywordstyle=\color{magenta}]

def hillclimb_local_search(base_solution) -> Solution:
	
	candidate_solution = generate_candidate(
		local_search_heuristics, base_solution
		)	
	
	if score(candidate_solution) < score(base_solution):
		return candidate_solution
	else:
		return base_solution
\end{lstlisting}

\subsubsection{Simulated Annealing}
The main idea of Simulated Annealing is that there is a possibility to pick the "worse" Solution, to get out of a local optimum.

This algorithm is described in detail in \cite{simulated annealing source}. 
In summary the worse solution can be picked with a probability, which decreases hyperbolically in every iteration.

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=2, keywordstyle=\color{magenta}]

def annealing_local_search(base_solution) -> Solution:
	
	candidate_solution = generate_candidate(
		local_search_heuristics, base_solution
		)	
	
	if score(candidate_solution) < score(base_solution):
		return candidate_solution
	elif random(0, 1) < (1 / iter_num):
		return candidate_solution
	else:
		return base_solution
\end{lstlisting}

\subsection*{Hyperparameters}
The above mentioned heuristics and metaheuristics have certain hyperparameters and in this section we detail which values for these parameters were found to work best.

\subsubsection{Hillclimb Hyperparameters}
Hillclimbing has two most important hyperparameters - depth and width.
These two values define the local neighborhood, which can be explored in each step, with depth corresponding to how many times the local search heuristics are applied and width to how many different candidates are considered.
With the VeRoLog Solver 2019 data sets and the problem simplification the most important parameter is depth, as the generated solutions are far from optimal, leading to long optimization runs.

These values were not rigorously tested, but comparisons of several values showed that using very low values of depth and width (less than 5) was inferior to using higher values, while increasing these values over 20 again resulted in inferior performance.
Thus we decided to use a depth of 5 and a width of 10.

\subsubsection{Annealing Hyperparameters}
Annealing has the same pair of hyperparameters as Hillclimbing (width and depth with these remaining the same for Annealing) but there is another one - cooling rate.
Cooling rate refers to how fast the temperature in the Annealing metaheuristic drops and how this influences the acceptance chance.
%TODO find some papers on different cooling rates 

The cooling rate used in this testing was hyperbolic \cite{simulated annealing survey} - the acceptance chance is inverse to the number of the iteration.

\subsubsection{Shaw Hyperparameters}
The Shaw heuristic has one important hyperparameter - similar request choice.
When choosing a pair of requests to swap the Shaw Heuristic generates only a list of similar requests, but then some of them have to be chosen.

More similar requests are preferred and the chance of picking a request drops exponentially with its position in the list.

\subsection{Request Selection}
\label{request selection}
In the Request Swap heuristic, two heuristics are used to determine which requests are to be swapped.
\subsubsection{Random Selection}
The requests are chosen at random from all the requests in the instance.
\subsubsection{Shaw Selection}
A true heuristic based approach, explained in Section \ref{shaw pair generation}.
All the requests are ordered by similarity to a randomly picked request before a pair of requests is picked from the list with a bias towards the most similar requests.
As noted in Section \ref{shaw pair generation} the weights of the requests drop exponentially with their position in the sorted list.

\section{Overall Algorithm}
In this section, we detail how all the parts of the overall algorithm are combined.

For generation, any of the three generators described in Section \ref{solution generation} can be used.
Due to the inherent nature in Random Tour Generation and Shaw Tour Generation, the generator itself is used to generate several hundred possible solutions and the best solution generated is used.

Optimization itself is performed by an iterative local search algorithm, where in each generation a Local Search Algorithm explores the neighborhood of the current solution and provides the solution for the next iteration.

\begin{lstlisting}[language=Python, basicstyle=\ttfamily\small, tabsize=4, keywordstyle=\color{magenta}]
def run_algorithm(instance) -> Solution:
	solution = generation.generate_solution(instance)

	while iter_number < iteration_count
		solution = 
			local_search_algorithm.optimize(solution)
	
	return solution
\end{lstlisting}
