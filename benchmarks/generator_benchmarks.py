from source import generator, instance_parser
from source import validator
from benchmarks import generation_plotter

ITERATION_NUM = 100


def run_all_generation_benchmarks(file_name):
    #print(f"Running generation benchmarks on file {file_name}")
    base_score = _run_base_generation_benchmark(file_name)
    random_tour_scores = _run_random_generation_benchmark(file_name)
    shaw_scores = _run_shaw_generation_benchmark(file_name)

    print(f"Base score is {base_score}, random tour average is {int(sum(random_tour_scores)/len(random_tour_scores))} and shaw average is {int(sum(shaw_scores)/len(shaw_scores))}")
    generation_plotter.extend_generation_benchmark_dataset(file_name, base_score, random_tour_scores, shaw_scores)


def _run_base_generation_benchmark(file_name):
    problem_desc = instance_parser.parse_file(file_name)
    base_solution = generator.generate_solution(problem_desc)
    solution_score = validator.get_solution_score(base_solution, problem_desc)
    return solution_score


def _run_random_generation_benchmark(file_name, iterations=ITERATION_NUM):
    scores = []
    problem_desc = instance_parser.parse_file(file_name)
    for _ in range(iterations):
        random_solution = generator.giant_tour_generation(problem_desc)
        solution_score = validator.get_solution_score(random_solution, problem_desc)
        scores.append(solution_score)
    return scores


def _run_shaw_generation_benchmark(file_name, iterations=ITERATION_NUM):
    scores = []
    problem_desc = instance_parser.parse_file(file_name)
    for _ in range(iterations):
        shaw_solution = generator.shaw_generation(problem_desc)
        solution_score = validator.get_solution_score(shaw_solution, problem_desc)
        scores.append(solution_score)
    return scores
