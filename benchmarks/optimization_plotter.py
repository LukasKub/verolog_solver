import os.path

import matplotlib.pyplot as plt
import sys
import numpy as np

plt.rcParams.update({'font.size': 30})

all_file_names = []

all_trivial_scores = []
all_base_scores = []

all_random_hillclimb_scores = []
all_shaw_hillclimb_scores = []
all_random_annealing_scores = []
all_shaw_annealing_scores = []


def _get_avg(seq):
    return sum(seq)/len(seq)


def _get_instace_from_file_name(file_name):
    name = "_".join(file_name.split("/")[1].split(".")[0].split("_")[1:])
    return name


def _rot(arr: [[int]]) -> [[int]]:
    out = []
    for line in arr:
        for i, item in enumerate(line):
            try:
                out[i].append(item)
            except:
                out.append([item])
    return out


def _transform_datasets(score_list: [[int]]) -> [int]:
    score_arr = _rot(score_list)
    out_array = []
    for gen_scores in score_arr:
        out_array.append(gen_scores)
    return out_array


def save_optimization_benchmark_data(file_name, trivial_score, base_score, random_hillclimb_scores: [[int]], shaw_hillclimb_scores: [[int]], random_annealing_scores: [[int]], shaw_annealing_scores: [[int]]):

    all_file_names.append(_get_instace_from_file_name(file_name))

    all_trivial_scores.append(trivial_score)
    all_base_scores.append(base_score)
    # all_random_annealing_scores.append(random_hillclimb_scores)
    # all_shaw_hillclimb_scores.append(shaw_hillclimb_scores)
    # all_random_annealing_scores.append(random_annealing_scores)
    # all_shaw_annealing_scores.append(shaw_annealing_scores)

    all_random_hillclimb_scores.append(_transform_datasets(random_hillclimb_scores))
    all_shaw_hillclimb_scores.append(_transform_datasets(shaw_hillclimb_scores))
    all_random_annealing_scores.append(_transform_datasets(random_annealing_scores))
    all_shaw_annealing_scores.append(_transform_datasets(shaw_annealing_scores))


def save_individual_plots():
    for file_name, trivial_score, base_score, random_hillclimb_scores, shaw_hillclimb_scores, random_annealing_scores, shaw_annealing_scores in zip(all_file_names, all_trivial_scores, all_base_scores, all_random_hillclimb_scores, all_shaw_hillclimb_scores, all_random_annealing_scores, all_shaw_annealing_scores):
        print(f"saving plot for {file_name}")
        plt.clf()
        plt.ticklabel_format(useOffset=False, style="plain")
        plt.plot(range(len(random_hillclimb_scores)), [_get_avg(i) for i in random_hillclimb_scores], antialiased=True, color="blue")
        plt.plot(range(len(shaw_hillclimb_scores)), [_get_avg(i) for i in shaw_hillclimb_scores], antialiased=True, color="orange")
        plt.plot(range(len(random_annealing_scores)), [_get_avg(i) for i in random_annealing_scores], antialiased=True, color="green")
        plt.plot(range(len(shaw_annealing_scores)), [_get_avg(i) for i in shaw_annealing_scores], antialiased=True, color="red")

        # plt.plot(range(len(random_hillclimb_scores)), [min(i) for i in random_hillclimb_scores], antialiased=True, color="blue")
        # plt.plot(range(len(shaw_hillclimb_scores)), [min(i) for i in shaw_hillclimb_scores], antialiased=True, color="orange")
        # plt.plot(range(len(random_annealing_scores)), [min(i) for i in random_annealing_scores], antialiased=True, color="green")
        # plt.plot(range(len(shaw_annealing_scores)), [min(i) for i in shaw_annealing_scores], antialiased=True, color="red")

        plt.legend(["Random hillclimb scores", "Shaw hillclimb scores", "Random annealing scores", "Shaw annealing scores"])

        plt.ylabel("Best solution score")
        plt.xlabel("Generation number")
        plt.gcf().set_size_inches(30, 18)

        plt.savefig(f"plots/{file_name}.png")

        f_handle = open(f"plots/{file_name}_data.txt", "w+")
        f_handle.write(f"{str(trivial_score)}\n")
        f_handle.write(f"{str(base_score)}\n")
        f_handle.write("\n".join([str(f) for f in random_hillclimb_scores]) + "\n")
        f_handle.write("\n".join([str(f) for f in shaw_hillclimb_scores]) + "\n")
        f_handle.write("\n".join([str(f) for f in random_annealing_scores]) + "\n")
        f_handle.write("\n".join([str(f) for f in shaw_annealing_scores]) + "\n")
        f_handle.close()

        # print(
        #     f"RH:{_get_avg(random_hillclimb_scores[-1])}\n"
        #     f"SH:{_get_avg(shaw_hillclimb_scores[-1])}\n"
        #     f"RA:{_get_avg(random_annealing_scores[-1])}\n"
        #     f"SA:{_get_avg(shaw_annealing_scores[-1])}"
        # )


def save_overall_plot():
    print("Saving overall plot")
    plt.clf()

    # sort scores by random tour score
    # all_scores = [(fn, ts/bs * 100, bs/bs * 100, _get_avg(rhs[-1])/bs * 100, _get_avg(shs[-1])/bs * 100, _get_avg(ras[-1])/bs * 100, _get_avg(sas[-1])/bs * 100) for fn, ts, bs, rhs, shs, ras, sas in zip(all_file_names, all_trivial_scores, all_base_scores, all_random_hillclimb_scores, all_shaw_hillclimb_scores, all_random_annealing_scores, all_shaw_annealing_scores)]
    all_scores = [(fn, ts/ts * 100, bs/ts * 100, _get_avg(rhs[-1])/ts * 100, _get_avg(shs[-1])/ts * 100, _get_avg(ras[-1])/ts * 100, _get_avg(sas[-1])/ts * 100) for fn, ts, bs, rhs, shs, ras, sas in zip(all_file_names, all_trivial_scores, all_base_scores, all_random_hillclimb_scores, all_shaw_hillclimb_scores, all_random_annealing_scores, all_shaw_annealing_scores)]
    # all_scores.sort(key=lambda k: k[3], reverse=True)
    all_scores.sort(key=lambda k: k[2], reverse=True)

    # transform list of tuples into list of lists in the right way
    file_names, trivial_score, base_scores, random_hill, shaw_hill, random_ann, shaw_ann = list(zip(*all_scores))
    X = np.arange(len(all_file_names))

    plt.plot(file_names, trivial_score, antialiased=True, color="black")
    plt.plot(file_names, base_scores, antialiased=True, color="royalblue")

    plt.bar(X - 0.4, random_hill, antialiased=True, width=0.2, color="blue")
    plt.bar(X - 0.2, shaw_hill, antialiased=True, width=0.2, color="orange")
    plt.bar(X, random_ann, antialiased=True, width=0.2, color="green")
    plt.bar(X + 0.2, shaw_ann, antialiased=True, width=0.2, color="red")

    # plt.legend(["Base optimization score", "Random hillclimb score", "Shaw hillclimb score", "Random annealing score", "Shaw annealing score", ], loc="lower left")
    plt.legend(["Trivial score", "Base optimization score", "Random hillclimb score", "Shaw hillclimb score", "Random annealing score", "Shaw annealing score", ], loc="lower left")

    # plt.xticks(rotation=45)
    plt.ylim(40, 102)
    # plt.ylabel("Percentage of base score")
    plt.ylabel("Percentage of trivial score")
    plt.xlabel("Instance number")
    plt.gcf().set_size_inches(30, 18)

    plt.savefig(f"optimization_graph_late.png")


def get_nums(line):
    tokens = line[1:len(line)-2].split(",")
    return [int(n) for n in tokens]


def interpret_file(file_name):
    print(f"Interpreting file {file_name}")
    f_handle = open(file_name)
    lines = f_handle.readlines()
    f_handle.close()

    line_num = len(lines) - 2

    all_file_names.append(file_name.split("/")[-1].split("_")[-2])

    all_trivial_scores.append(int(lines[0]))
    all_base_scores.append(int(lines[1]))

    lines = lines[2:]

    all_random_hillclimb_scores.append([get_nums(l) for l in lines[:int(line_num / 4)]][-5:])
    all_shaw_hillclimb_scores.append([get_nums(l) for l in lines[int(line_num / 4):int(line_num / 2)]][-5:])
    all_random_annealing_scores.append([get_nums(l) for l in lines[int(line_num / 2):int(3 * line_num / 4)]][-5:])
    all_shaw_annealing_scores.append([get_nums(l) for l in lines[int(3 * line_num / 4):]][-5:])
    print(f"File {file_name} interpreted")


def parse_file(file_name):
    if os.path.isdir(file_name):
        for file in sorted(os.listdir(file_name)):
            parse_file(file_name + "/" + file)
    else:
        if "data.txt" in file_name:
            interpret_file(file_name)


if __name__ == "__main__":
    input_file_name = sys.argv[1]
    parse_file(input_file_name)
    save_overall_plot()
