import matplotlib.pyplot as plt
import numpy as np

plt.rcParams.update({'font.size': 30})

instance_dataset = []
all_file_names = []
all_base_scores = []
all_random_tour_scores = []
all_shaw_scores = []

bot_random = []
top_random = []
bot_shaw = []
top_shaw = []


def _get_avg(seq):
    return sum(seq)/len(seq)


def _get_instace_from_file_name(file_name):
    instance_dataset.append("_".join(file_name.split("/")[1].split(".")[0].split("_")[1:3]))
    name = "_".join(file_name.split("/")[1].split(".")[0].split("_")[3:])
    return name


def extend_generation_benchmark_dataset(file_name, base_score: int, random_tour_scores: [int], shaw_scores: [int]):
    all_file_names.append(_get_instace_from_file_name(file_name))
    all_base_scores.append(100)
    # all_random_tour_scores.append(_get_avg(random_tour_scores)/base_score*100)
    # all_shaw_scores.append(_get_avg(shaw_scores)/base_score*100)
    all_random_tour_scores.append([s/base_score * 100 for s in random_tour_scores])
    all_shaw_scores.append([s/base_score * 100 for s in shaw_scores])
    bot_random.append(min(random_tour_scores)/base_score * 100)
    top_random.append(max(random_tour_scores)/base_score * 100)
    bot_shaw.append(min(shaw_scores)/base_score * 100)
    top_shaw.append(max(shaw_scores)/base_score * 100)

    # bot_25_random.append(sorted(random_tour_scores)[int(len(random_tour_scores) * 25 / 100)]/base_score * 100)
    # top_25_random.append(sorted(random_tour_scores)[int(len(random_tour_scores) * 75 / 100)]/base_score * 100)
    # bot_25_shaw.append(sorted(shaw_scores)[int(len(shaw_scores) * 25 / 100)]/base_score * 100)
    # top_25_shaw.append(sorted(shaw_scores)[int(len(shaw_scores) * 75 / 100)]/base_score * 100)


def save_plot():
    plt.clf()

    # sort scores by random tour score
    all_scores = [(fn, bas, rts, ss, br, tr, bs, ts) for fn, bas, rts, ss, br, tr, bs, ts in zip(all_file_names, all_base_scores, all_random_tour_scores, all_shaw_scores, bot_random, top_random, bot_shaw, top_shaw)]
    all_scores.sort(key=lambda k: _get_avg(k[2]), reverse=True)

    # transform list of tuples into list of lists in the right way
    file_names, base_scores, random_tour_scores, shaw_scores, br, tr, bs, ts = list(zip(*all_scores))
    X = np.arange(0, len(file_names))
    bs, = plt.plot(X + 1, base_scores, color="b", antialiased=True)
    b1 = plt.boxplot(random_tour_scores, patch_artist=True, showfliers=False)
    b2 = plt.boxplot(shaw_scores, patch_artist=True, showfliers=False)

    plt.xticks(X + 1, file_names)

    for patch in b1['boxes']:
        patch.set_facecolor("forestgreen")

    for patch in b2['boxes']:
        patch.set_facecolor("tomato")

    plt.legend([bs, b1["boxes"][0], b2["boxes"][0]], ["Trivial Generator score", "Random Tour Generator score", "Shaw Tour Generator score"], loc='lower left')
    #
    # plt.legend(["Trivial Generator score", "Random Tour Generator average score", "Shaw Tour Generator average score"], loc="lower left")

    plt.ylim(40, 110)
    plt.xlim(0, len(file_names) + 1)
    plt.ylabel("Percentage of Base score")
    plt.xlabel("Instance number")
    plt.gcf().set_size_inches(30, 18)

    plt.savefig(f"generation_graph_{instance_dataset[0]}.png")
