from source import optimization, instance_parser
from source import generator
from benchmarks import optimization_plotter
import multiprocessing
from source import validator
from utils import logger
import time

ITERATION_COUNT = 8
ITERATION_LEN = 50 * 1000
PROCESS_COUNT = 8
LOG_INTERVAL = 1 * 1000


def _reset_caches():
    import source.local_search.shaw_simmilarity as shaw_sim
    shaw_sim.shaw_list_dict = shaw_sim.defaultdict(int)
    import utils.utils as utils
    utils.journey_length_dict = utils.defaultdict(int)
    utils.best_path_dict = utils.defaultdict(int)


def run_all_optimization_benchmarks(file_name):
    _reset_caches()
    problem_desc = instance_parser.parse_file(file_name)
    trivial_solution = generator.generate_solution(problem_desc)
    starting_solution = generator.advanced_shaw_generation(problem_desc, 100)
    # starting_solution = generator.advanced_random_generation(problem_desc, 100)

    random_hillclimb_scores = run_random_hillclimb_benchmark(file_name, starting_solution)
    shaw_hillclimb_scores = run_shaw_hillclimb_benchmark(file_name, starting_solution)
    random_annealing_scores = run_random_annealing_benchmark(file_name, starting_solution)
    shaw_annealing_scores = run_shaw_annealing_benchmark(file_name, starting_solution)

    optimization_plotter.save_optimization_benchmark_data(file_name, validator.get_solution_score(trivial_solution, problem_desc), validator.get_solution_score(starting_solution, problem_desc), random_hillclimb_scores, shaw_hillclimb_scores, random_annealing_scores, shaw_annealing_scores)

    f_handle = open("optimization_results.tex", "a+")
    instance_name = " ".join(file_name.split("/")[-1].split(".")[0].split("_")[2:])
    f_handle.write(f"{instance_name}&{'&'.join([str(validator.get_solution_score(trivial_solution, problem_desc)),str(validator.get_solution_score(starting_solution, problem_desc)),str(min(random_hillclimb_scores[-1])),str(min(shaw_hillclimb_scores[-1])),str(min(random_annealing_scores[-1])),str(min(shaw_annealing_scores[-1]))])}\\\\\n")
    f_handle.close()


def run_random_hillclimb_benchmark(file_name, base_solution):
    problem_desc = instance_parser.parse_file(file_name)
    all_scores = []

    start_time = time.time()

    with multiprocessing.Pool(processes=PROCESS_COUNT) as pool:
        algorithm = optimization.RandomHillclimbAlgorithm(problem_desc, iterations=ITERATION_LEN, log_interval=LOG_INTERVAL,
                                                          mode="RELEASE")
        futures = [
            pool.apply_async(
                _benchmark_algorithm,
                (algorithm, base_solution,)
            )
            for _ in range(ITERATION_COUNT)
        ]

        for f in futures:
            scores = f.get()
            all_scores.append(scores)

    duration = round(time.time() - start_time, 2)
    logger.info(f"Random hillclimb took {duration}s")

    return all_scores


def run_shaw_hillclimb_benchmark(file_name, base_solution):
    problem_desc = instance_parser.parse_file(file_name)
    all_scores = []

    start_time = time.time()

    with multiprocessing.Pool(processes=PROCESS_COUNT) as pool:
        algorithm = optimization.SimilarHillclimbAlgorithm(problem_desc, iterations=ITERATION_LEN, log_interval=LOG_INTERVAL,
                                                           mode="RELEASE")
        futures = [
            pool.apply_async(
                _benchmark_algorithm,
                (algorithm, base_solution,)
            )
            for _ in range(ITERATION_COUNT)
        ]

        for f in futures:
            scores = f.get()
            all_scores.append(scores)

    duration = round(time.time() - start_time, 2)
    logger.info(f"Shaw hillclimb took {duration}s")

    return all_scores


def run_random_annealing_benchmark(file_name, base_solution):
    problem_desc = instance_parser.parse_file(file_name)
    all_scores = []

    start_time = time.time()

    with multiprocessing.Pool(processes=PROCESS_COUNT) as pool:
        algorithm = optimization.RandomAnnealingAlgorithm(problem_desc, iterations=ITERATION_LEN, log_interval=LOG_INTERVAL,
                                                          mode="RELEASE")
        futures = [
            pool.apply_async(
                _benchmark_algorithm,
                (algorithm, base_solution,)
            )
            for _ in range(ITERATION_COUNT)
        ]

        for f in futures:
            scores = f.get()
            all_scores.append(scores)

    duration = round(time.time() - start_time, 2)
    logger.info(f"Random annealing took {duration}s")

    return all_scores


def run_shaw_annealing_benchmark(file_name, base_solution):
    problem_desc = instance_parser.parse_file(file_name)
    all_scores = []

    start_time = time.time()

    with multiprocessing.Pool(processes=PROCESS_COUNT) as pool:
        algorithm = optimization.SimilarAnnealingAlgorithm(problem_desc, iterations=ITERATION_LEN, log_interval=LOG_INTERVAL,
                                                           mode="RELEASE")
        futures = [
            pool.apply_async(
                _benchmark_algorithm,
                (algorithm, base_solution,)
            )
            for _ in range(ITERATION_COUNT)
        ]

        for f in futures:
            scores = f.get()
            all_scores.append(scores)

    duration = round(time.time() - start_time, 2)
    logger.info(f"Shaw annealing took {duration}s")

    return all_scores


def _benchmark_algorithm(algorithm, trivial_solution):
    best_solution = algorithm.optimize(trivial_solution)
    return algorithm.get_scores()
