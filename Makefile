all:
	make get_instances unit_test run
unit_test:
	python3 unit_tests.py -v
run:
	python3 runner.py ORTEC-small/VSC2019_ORTEC_Small_01.txt
benchmark:
	python3 benchmark.py ORTEC-early/ --gen --opt
get_instances:
	if [ ! -d ORTEC-early ]; then wget https://verolog2019.ortec.com/instance/2/ORTEC-early.zip; unzip ORTEC-early.zip; rm ORTEC-early.zip; mkdir ORTEC-early; mv VSC*.txt ORTEC-early/; fi
	if [ ! -d ORTEC-small ]; then wget https://verolog2019.ortec.com/instance/5/ORTEC-small.zip; unzip ORTEC-small.zip; rm ORTEC-small.zip; mkdir ORTEC-small; mv VSC*.txt ORTEC-small/; fi
	if [ ! -d ORTEC-late ]; then wget https://verolog2019.ortec.com/instance/3/ORTEC-late.zip; unzip ORTEC-late.zip; rm ORTEC-late.zip; mkdir ORTEC-late; mv VSC*.txt ORTEC-late/; fi
	if [ ! -d ORTEC-hidden ]; then wget https://verolog2019.ortec.com/instance/4/ORTEC-hidden.zip; unzip ORTEC-hidden.zip; rm ORTEC-hidden.zip; mkdir ORTEC-hidden; mv VSC*.txt ORTEC-hidden/; fi
