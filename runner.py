import cProfile
import argparse

from source import generator
from source import instance_parser
from source import optimization


def run_optimization(file_name, annealing, shaw, iter_num, mode):
    problem_desc = instance_parser.parse_file(file_name)
    base_solution = generator.advanced_shaw_generation(problem_desc, 100)

    if annealing:
        if shaw:
            optimizerclass = optimization.SimilarAnnealingAlgorithm(problem_desc, iter_num, 10, mode)
        else:
            optimizerclass = optimization.RandomAnnealingAlgorithm(problem_desc, iter_num, 10, mode)
    else:
        if shaw:
            optimizerclass = optimization.SimilarHillclimbAlgorithm(problem_desc, iter_num, 10, mode)
        else:
            optimizerclass = optimization.RandomHillclimbAlgorithm(problem_desc, iter_num, 10, mode)

    final_solution = optimizerclass.optimize(base_solution)
    print(final_solution)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Script for solving a VeRoLog Challenge 2019 instance')
    parser.add_argument('file_name', metavar='file_name', type=str,
                        help='The file containing the instance')
    parser.add_argument('--annealing', dest='annealing', action='store_true',
                        help='Run the annealing local search algorithm (default is hillclimb)')
    parser.add_argument('--shaw', dest='shaw', action='store_true',
                        help='Also use the shaw heuristic')
    parser.add_argument('--run-debug', dest='run_debug', action='store_true',
                        help='Run the debug version with more checks and logging')
    parser.add_argument('--iter', dest='iter_num', type=int,
                        help='The number of iterations for the algorithm')
    parser.add_argument('--profile', dest='profile', action='store_true',
                        help='Profile the software using cProfile')

    args = parser.parse_args()

    if args.run_debug:
        MODE = "DEBUG"
    else:
        MODE = "RELEASE"

    if args.iter_num is None:
        args.iter_num = 50 * 1000

    if args.profile:
        cProfile.run(f"run_optimization({args.file_name}, {args.annealing}, {args.shaw}, {args.iter_num}, {MODE})", sort = "tottime")
    else:
        run_optimization(args.file_name, args.annealing, args.shaw, args.iter_num, MODE)
