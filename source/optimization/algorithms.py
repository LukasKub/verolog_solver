from source.optimization.base_class import OptimizeBaseClass
from source.optimization.local_search import *
from source.optimization.local_optimization import *

from utils import description
from utils import logger
from source import validator

"""
full optimization algorithms
"""

CANDIDATE_COUNT = 10
NEIGHBORHOOD_DEPTH = 5


class AlgorithmBaseClass(OptimizeBaseClass):
    def __init__(self,
                 problem_desc: description.ProblemDescription,
                 local_search_class: LocalSearchClass,
                 iterations=10,
                 log_interval=1,
                 mode="DEBUG"
                 ):
        super().__init__(problem_desc)
        self.local_search_class = local_search_class
        self.iterations = iterations
        self.log_interval = log_interval
        self.mode = mode
        self.scores = []

    def optimize(self, solution: description.Solution) -> description.Solution:
        try:
            best_solution = solution
            for i in range(self.iterations):
                best_solution = self.local_search_class.optimize(best_solution)
                self.scores.append(validator.get_solution_score(best_solution, self.problem_desc))
                if i % self.log_interval == 0:
                    logger.info(f"Iteration {i}, solution_score:{validator.get_solution_score(best_solution, self.problem_desc)}, solution valid:{validator.check_solution_validity(best_solution, self.problem_desc, True)}")

            logger.info(f"Final solution score:{validator.get_solution_score(best_solution, self.problem_desc)}, solution valid:{validator.check_solution_validity(solution, self.problem_desc, True)}")
            return best_solution
        except RuntimeError as re:
            print(f"Encountered an error: {re}\n"
                  f"Solution: {best_solution}")
            raise

    def get_scores(self):
        return self.scores


class RandomHillclimbAlgorithm(AlgorithmBaseClass):
    def __init__(self,
                 problem_desc: description.ProblemDescription,
                 iterations=10,
                 log_interval=1,
                 mode="DEBUG",
                 ):
        super().__init__(problem_desc, iterations=iterations,
                         local_search_class=
                         HillclimbCheckClass(
                                 problem_desc,
                                 [
                                     RandomStepClass(problem_desc,
                                                     [
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                         MoveRandomRequestClass(problem_desc, mode),
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                     ]
                                                     ),
                                 ],
                                 candidate_count=CANDIDATE_COUNT,
                                 depth=NEIGHBORHOOD_DEPTH,
                             ),
                         log_interval=log_interval,
                         mode=mode
                         )


class SimilarHillclimbAlgorithm(AlgorithmBaseClass):
    def __init__(self,
                 problem_desc: description.ProblemDescription,
                 iterations=10,
                 log_interval=1,
                 mode="DEBUG"
                 ):
        super().__init__(problem_desc, iterations=iterations,
                         local_search_class=
                         HillclimbCheckClass(
                                 problem_desc,
                                 [
                                     RandomStepClass(problem_desc,
                                                     [
                                                         MoveRandomRequestClass(problem_desc, mode),
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                         SwitchSimilarRequestsClass(problem_desc, mode),
                                                     ]
                                                     ),
                                 ],
                                 candidate_count=CANDIDATE_COUNT,
                                 depth=NEIGHBORHOOD_DEPTH,
                         ),
                         log_interval=log_interval,
                         mode=mode
                         )


class RandomAnnealingAlgorithm(AlgorithmBaseClass):
    def __init__(self,
                 problem_desc: description.ProblemDescription,
                 iterations=10,
                 log_interval=1,
                 mode="DEBUG"
                 ):
        super().__init__(problem_desc, iterations=iterations,
                         local_search_class=
                         AnnealingCheckClass(
                                 problem_desc,
                                 [
                                     RandomStepClass(problem_desc,
                                                     [
                                                         MoveRandomRequestClass(problem_desc, mode),
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                     ]
                                                     ),
                                 ],
                                 base_temperature=iterations,
                                 candidate_count=CANDIDATE_COUNT,
                                 depth=NEIGHBORHOOD_DEPTH,
                         ),
                         log_interval=log_interval,
                         mode=mode
                         )


class SimilarAnnealingAlgorithm(AlgorithmBaseClass):
    def __init__(self,
                 problem_desc: description.ProblemDescription,
                 iterations=10,
                 log_interval=1,
                 mode="DEBUG",
                 ):
        super().__init__(problem_desc, iterations=iterations,
                         local_search_class=
                         AnnealingCheckClass(
                                 problem_desc,
                                 [
                                     RandomStepClass(problem_desc,
                                                     [
                                                         MoveRandomRequestClass(problem_desc, mode),
                                                         SwitchRandomRequestsClass(problem_desc, mode),
                                                         SwitchSimilarRequestsClass(problem_desc, mode),
                                                     ]
                                                     ),
                                 ],
                                 base_temperature=iterations,
                                 candidate_count=CANDIDATE_COUNT,
                                 depth=NEIGHBORHOOD_DEPTH,
                         ),
                         log_interval=log_interval,
                         mode=mode,
                         )
