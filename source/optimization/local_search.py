from source.optimization.base_class import OptimizeBaseClass
from source.optimization.local_optimization import LocalOptimizationClass
from utils import description
from source import validator
from source import local_search

"""
boundary classes
"""


class LocalSearchClass(OptimizeBaseClass):
    def __init__(
            self,
            problem_desc: description.ProblemDescription,
            local_optimize_class_list: [LocalOptimizationClass],
            boundary_function,
            candidate_count: int = 10,
            depth: int = 1,
    ):
        super().__init__(problem_desc)
        self.boundary_function = boundary_function
        self.candidate_count = candidate_count
        self.local_optimize_class_list = local_optimize_class_list
        self.depth = depth

    @staticmethod
    def get_score(itm, problem_desc):
        return validator.get_solution_score(itm, problem_desc)

    def optimize(self, solution: description.Solution) -> description.Solution:
        best_solution = description.Solution(solution.journeys)
        candidates = []
        for _ in range(self.candidate_count):
            new_candidate = description.Solution(solution.journeys)
            for _ in range(self.depth):
                for local_optimize_class in self.local_optimize_class_list:
                    new_candidate = local_optimize_class.optimize(new_candidate)
                    candidates.append(description.Solution(new_candidate.journeys))

        candidate_solution = min(
            candidates,
            key=lambda itm: self.get_score(itm, self.problem_desc)
        )

        best_solution = self.boundary_function(candidate_solution, best_solution, self.problem_desc)

        return best_solution


class HillclimbCheckClass(LocalSearchClass):
    def __init__(
            self,
            problem_desc: description.ProblemDescription,
            local_optimize_class_list: [LocalOptimizationClass],
            candidate_count: int = 10,
            depth: int = 10,
    ):
        super().__init__(problem_desc, local_optimize_class_list,
                         candidate_count=candidate_count,
                         boundary_function=local_search.hillclimb_check,
                         depth=depth)


class AnnealingCheckClass(LocalSearchClass):
    def __init__(
            self,
            problem_desc: description.ProblemDescription,
            local_optimize_class_list: [LocalOptimizationClass],
            base_temperature: int = 10,
            candidate_count: int = 10,
            depth: int = 1,
                ):
        super().__init__(problem_desc, local_optimize_class_list,
                         candidate_count=candidate_count,
                         boundary_function=local_search.SimulatedAnnealing(base_temperature),
                         depth=depth)
