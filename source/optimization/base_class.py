from utils import description


class OptimizeBaseClass:
    def __init__(self, problem_desc: description.ProblemDescription):
        self.problem_desc = problem_desc
        pass

    def optimize(self, solution: description.Solution) -> description.Solution:
        return solution
