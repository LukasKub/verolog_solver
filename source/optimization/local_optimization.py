import random

from source.optimization.base_class import OptimizeBaseClass

from utils import description
from utils import logger

from source import local_search
from source import validator

"""
local search classes
"""


class LocalOptimizationClass(OptimizeBaseClass):
    def __init__(self, problem_desc: description.ProblemDescription, local_search_func, mode="DEBUG"):
        super().__init__(problem_desc)
        self.local_search_function = local_search_func
        self.mode = mode

    def optimize(self, solution: description.Solution) -> description.Solution:
        if self.mode == "DEBUG":
            try:
                logger.debug(f"Running function {self.local_search_function}")
                new_solution = self.local_search_function(solution, self.problem_desc)
                validator.check_solution_validity(new_solution, self.problem_desc)
            except RuntimeError as e:
                logger.error(f"Encountered error during optimization:\n{e}\n"
                             # f"Current solution:{solution}"
                             )
                raise
            return new_solution

        elif self.mode == "RELEASE":
            return self.local_search_function(solution, self.problem_desc)

        else:
            raise RuntimeError("Did not specify right mode for local optimization")


#class ConditionalFragmentClass(LocalOptimizationClass):
#    def __init__(self, problem_desc: description.ProblemDescription,
#                 probability=0.5, mode="RELEASE"):
#        super().__init__(problem_desc, local_search.fragment_random_journey, mode)
#        self.probability = probability
#
#    def optimize(self, solution: description.Solution) -> description.Solution:
#        if random.random() < self.probability:
#            return super(ConditionalFragmentClass, self).optimize(solution)
#        else:
#            return solution


#class FragmentRandomJourneyClass(LocalOptimizationClass):
#    def __init__(self, problem_desc: description.ProblemDescription, mode="DEBUG"):
#        super().__init__(problem_desc, local_search.fragment_random_journey, mode)


class MoveRandomRequestClass(LocalOptimizationClass):
    def __init__(self, problem_desc: description.ProblemDescription, mode="DEBUG"):
        super().__init__(problem_desc, local_search.move_random_request, mode)


class SwitchRandomRequestsClass(LocalOptimizationClass):
    def __init__(self, problem_desc: description.ProblemDescription, mode="DEBUG"):
        super().__init__(problem_desc, local_search.swap_random_requests, mode)


class SwitchSimilarRequestsClass(LocalOptimizationClass):
    def __init__(self, problem_desc: description.ProblemDescription, mode="DEBUG"):
        super().__init__(problem_desc, local_search.swap_requests_similar, mode)


class RandomStepClass(OptimizeBaseClass):
    def __init__(self, problem_desc: description.ProblemDescription, local_classes: [LocalOptimizationClass]):
        super().__init__(problem_desc)
        self.classes = local_classes

    def optimize(self, solution: description.Solution) -> description.Solution:
        chosen_class = random.choice(self.classes)
        return chosen_class.optimize(solution)
