import sys

from utils import description
from utils import logger


def parse_file(file_name):
    """
    Simple parsing method

    :param file_name: name/path to file containing verolog 2019 data
    :return: problem description as defined by the class
    """
    logger.info(f"Parsing file {file_name}")

    problem_desc = description.ProblemDescription()
    problem_desc.requests = []
    problem_desc.locations = []
    problem_desc.machines = []
    machine_weights = []

    parser_state = ""
    f_handle = open(file_name, "r")
    for line in f_handle.readlines():
        line = line[:len(line)-1]

        if line == "":
            pass
        else:
            words = line.split()
            if words[0] == "DATASET":
                problem_desc.dataset_name = " ".join(words[2:])
                continue
            if words[0] == "NAME":
                # problem dataset and name consist of several words, so just join them before adding to dictionary
                problem_desc.instance_name = " ".join(words[2:])
                continue

            if words[0] in ["MACHINES", "LOCATIONS", "REQUESTS", "TECHNICIANS"]:
                parser_state = words[0]
                continue

            if parser_state == "" and words[1] == "=":
                # standard state description (Technician cost and similar)
                subject = words[0]
                value = int(words[2])
                if subject == "TRUCK_CAPACITY":
                    problem_desc.truck_capacity = value
                elif subject == "TRUCK_COST":
                    problem_desc.truck_cost = value
                elif subject == "TRUCK_DAY_COST":
                    problem_desc.truck_day_cost = value
                elif subject == "TRUCK_DISTANCE_COST":
                    problem_desc.truck_distance_cost = value
                elif subject == "TRUCK_MAX_DISTANCE":
                    problem_desc.truck_max_distance = value
            else:
                words = [int(i) for i in words]
                if parser_state == "LOCATIONS":
                    loc = description.Location(words[0], words[1], words[2])
                    problem_desc.locations.append(loc)
                elif parser_state == "REQUESTS":
                    req = description.Request(words[0], words[1], words[2], words[3], words[4], words[5], problem_desc)
                    problem_desc.requests.append(req)
                elif parser_state == "MACHINES":
                    machine = description.Machine(words[0], words[1])
                    problem_desc.machines.append(machine)

    logger.debug(f"state is represented by descriptor: {problem_desc}")
    return problem_desc


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <name of file to be parsed>")
        exit(1)
    print(f"Parsing {sys.argv[1]}")
    problem_desc = parse_file(sys.argv[1])
    for r in problem_desc.requests:
        print(r)
