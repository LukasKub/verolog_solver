from utils import description
from utils import utils
from utils import logger
import source.local_search.helper_functions as helper_functions


def move_random_request(solution: description.Solution, problem_desc: description.ProblemDescription) -> description.Solution:
    source_index, destination_index = utils.generate_index_pair(solution.journeys)

    request_index = utils.generate_index(solution.journeys[source_index].requests)

    return _move_request(solution, source_index, request_index, destination_index, problem_desc)


def _move_request(solution: description.Solution, source_index, request_index, destination_index, problem_desc: description.ProblemDescription) \
        -> description.Solution:
    """
    from the source_index journey move the request_index request to the destination_index journey
    all indexes are true indexes, not ids (do not need to be offset by 1)
    :return: updated solution if it is better
    """
    source_journey = solution.journeys[source_index]
    source_request = source_journey.requests[request_index]
    destination_journey = solution.journeys[destination_index]

    new_source_journey = helper_functions.remove_request(source_journey, source_request, problem_desc)
    new_destination_journey = helper_functions.assimilate_request(destination_journey, source_request, problem_desc)

    if new_destination_journey == -1 or new_source_journey == -1:
        # new journeys are not valid
        return solution

    new_journeys = [journey for journey in solution.journeys if journey not in [source_journey, destination_journey]]

    if new_source_journey.requests:
        # don't add source journey if there are no request in the journey
        new_journeys.append(new_source_journey)
    new_journeys.append(new_destination_journey)

    for location in destination_journey.path:
        if location not in new_destination_journey.path:
            raise RuntimeError(f"Did not add request to journey:\n"
                               f"Orig:{destination_journey}\n"
                               f"New:{new_destination_journey}\n"
                               f"Request id:{source_request}")
    new_solution = description.Solution(new_journeys)

    return new_solution
