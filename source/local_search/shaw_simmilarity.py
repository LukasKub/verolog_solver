from utils import description
from utils import utils
import operator
import random
from collections import defaultdict

shaw_list_dict = defaultdict(list)


def generate_shaw_list(problem_desc: description.ProblemDescription) -> [description.Request]:
    first_request = random.choice(problem_desc.requests)
    if not shaw_list_dict[first_request.id]:
        shaw_list_dict[first_request.id] = shaw_generation(first_request.id, problem_desc)

    return shaw_list_dict[first_request.id]


def shaw_generation(first_request_id: int, problem_desc: description.ProblemDescription) -> [description.Request]:
    first_request = problem_desc.requests[first_request_id - 1]

    request_list = []
    for request in problem_desc.requests:
        distance = utils.get_location_distance(first_request.location_id - 1, request.location_id - 1, problem_desc)
        # weight_diff = abs(request.weight - (problem_desc.truck_capacity - first_request.weight))
        weight_diff = abs(request.weight - first_request.weight)
        request_list.append((distance, weight_diff, request))

    request_list.sort(key=operator.itemgetter(1, 0))
    # request_list.sort(key=operator.itemgetter(0, 1))

    return [request_tuple[2] for request_tuple in request_list]
