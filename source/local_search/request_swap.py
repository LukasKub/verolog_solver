from utils import description
from utils import utils
import source.local_search.helper_functions as helper_functions


def swap_requests_similar(solution: description.Solution, problem_desc: description.ProblemDescription) -> description.Solution:
    first_index_pair, second_index_pair = helper_functions.get_simmilar_requests(solution, problem_desc)
    return _swap_requests(solution, first_index_pair, second_index_pair, problem_desc)


def swap_random_requests(solution: description.Solution, problem_desc: description.ProblemDescription) -> description.Solution:
    first_index, second_index = utils.generate_index_pair(solution.journeys)

    first_req_index = utils.generate_index(solution.journeys[first_index].requests)
    second_req_index = utils.generate_index(solution.journeys[second_index].requests)

    return _swap_requests(solution, (first_index, first_req_index), (second_index, second_req_index), problem_desc)


def _swap_requests(solution: description.Solution, first_index_pair, second_index_pair, problem_desc: description.ProblemDescription) \
        -> description.Solution:
    """
    switch two requests from two journeys
    all indexes are true indexes, not ids (do not need to be offset by 1)
    :return: updated solution if it is better
    """
    first_jour_index, first_req_index = first_index_pair
    second_jour_index, second_req_index = second_index_pair

    first_journey = solution.journeys[first_jour_index]
    first_request = first_journey.requests[first_req_index]
    second_journey = solution.journeys[second_jour_index]
    second_request = second_journey.requests[second_req_index]

    new_first_journey = \
        helper_functions.assimilate_request(
            helper_functions.remove_request(
                first_journey, first_request, problem_desc),
            second_request, problem_desc)

    new_second_journey = \
        helper_functions.assimilate_request(
            helper_functions.remove_request(
                second_journey, second_request, problem_desc),
            first_request, problem_desc)

    if new_first_journey == -1 or new_second_journey == -1:
        # some adding/removing is incorrect
        return solution

    new_journeys = [journey for journey in solution.journeys if (journey != first_journey) and (journey != second_journey)]
    new_journeys.append(new_first_journey)
    new_journeys.append(new_second_journey)
    new_solution = description.Solution(new_journeys)

    return new_solution
