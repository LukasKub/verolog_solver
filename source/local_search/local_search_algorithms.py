from random import random

from source import validator
from utils import description


debug = False


class SimulatedAnnealing:
    def __init__(self, temperature):
        self.temperature: float = float(temperature + 1)
        self.base_temperature: float = self.temperature

    def acceptance_prob(self, orig_value, new_value):
        self.temperature -= 1

        if new_value > orig_value:
            # new value is worse than old value
            chance = 1.0 / (self.base_temperature - self.temperature + 1)
            return chance
        else:
            # new value is better than old value
            return 1

    def __call__(self, original_s: description.Solution, new_s: description.Solution, problem_desc: description.ProblemDescription, *args, **kwargs) -> description.Solution:
        if debug:
            original_validity = validator.check_solution_validity(new_s, problem_desc)
            new_validity = validator.check_solution_validity(new_s, problem_desc)

            if not (original_validity and new_validity):
                raise RuntimeError("Got two bad solutions")

            if not new_validity:
                return original_s
            if not original_validity:
                return new_s

        original_score = validator.get_solution_score(original_s, problem_desc)
        new_score = validator.get_solution_score(new_s, problem_desc)

        if random() < self.acceptance_prob(original_score, new_score):
            return new_s
        else:
            return original_s


def hillclimb_check(original_s: description.Solution, new_s: description.Solution, problem_desc: description.ProblemDescription) -> description.Solution:
    """
    :return: 1 if sol1 is better, 2 if sol2, -1 if both are invalid
    """
    if debug:
        original_validity = validator.check_solution_validity(new_s, problem_desc)
        new_validity = validator.check_solution_validity(new_s, problem_desc)

        if not (original_validity and new_validity):
            raise RuntimeError("Got two bad solutions")

        if not new_validity:
            return original_s
        if not original_validity:
            return new_s

    original_score = validator.get_solution_score(original_s, problem_desc)
    new_score = validator.get_solution_score(new_s, problem_desc)

    return original_s if original_score < new_score else new_s
