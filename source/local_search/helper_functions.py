import numpy as np

from utils import description
from utils import utils
from source.local_search import shaw_simmilarity
import random
import math


def assimilate_request(journey: description.Journey, request, problem_desc: description.ProblemDescription):
    """
    Add request to journey with shortest path possible
    :return: optimized journey if addition is possible, -1 otherwise
    """
    if journey == -1:
        return -1

    req_id = utils.get_id(request)
    true_request = problem_desc.requests[req_id - 1]

    req_loc_id = true_request.location_id
    request_weight = true_request.weight

    if request_weight + journey.weight > problem_desc.truck_capacity:
        return -1

    if not journey.requests:
        return description.Journey(request_weight,
                                   utils.calculate_journey_length([1, req_loc_id, 1], problem_desc),
                                   [1, req_loc_id, 1], [req_id])

    path, length = utils.add_location_to_path(journey.path, req_loc_id, problem_desc)

    if not path or length > problem_desc.truck_max_distance:
        return -1

    new_requests = [r for r in journey.requests] + [req_id]

    return description.Journey(request_weight + journey.weight, length, path, new_requests)


def remove_request(journey: description.Journey, request, problem_desc: description.ProblemDescription):
    """
    remove request from journey and rearrange path to shortest possible
    """
    if journey == -1 or request not in journey.requests:
        return -1

    if len(journey.requests) == 1:
        return description.Journey(0, 0, [], [])

    req_id = utils.get_id(request)
    true_request = problem_desc.requests[req_id - 1]
    request_loc = true_request.location_id

    path, length = utils.remove_loc_from_path(journey.path, request_loc, problem_desc)

    if not path or length > problem_desc.truck_max_distance:
        return -1

    request_weight = true_request.weight
    new_requests = [r for r in journey.requests if utils.get_id(r) != req_id]

    return description.Journey(journey.weight - request_weight, length, path, new_requests)


def get_simmilar_requests(solution: description.Solution, problem_desc: description.ProblemDescription) -> ((int, int), (int, int)):
    request_list = shaw_simmilarity.generate_shaw_list(problem_desc)
    i, j = _pick_simmilar_requests(request_list)

    return _find_indexes_for_request(solution, request_list[i], request_list[j])


def _pick_simmilar_requests2(request_list):
    i = 0
    while True:
        for j in range(len(request_list)):
            if random.random() < 1 / (j + 2):
                return i, j


def _pick_simmilar_requests(request_list):
    i = 0
    pick = random.random()
    while pick < 1.0 / (2 ** len(request_list)):
        pick = random.random()

    j = - int(np.floor(np.log(pick) / np.log(2)))
    return i, j


def _find_indexes_for_request(solution: description.Solution, first_request: description.Request, second_request: description.Request):
    first_request_tuple = None
    second_request_tuple = None
    for i, journey in enumerate(solution.journeys):
        for j, candidate_request in enumerate(journey.requests):
            if first_request.id == candidate_request:
                first_request_tuple = (i, j)
            if second_request.id == candidate_request:
                second_request_tuple = (i, j)

        if first_request_tuple is not None and second_request_tuple is not None:
            return first_request_tuple, second_request_tuple
