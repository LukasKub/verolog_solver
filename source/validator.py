from utils import utils
from utils import description
from utils import logger


def check_solution_validity(solution: description.Solution, problem_desc: description.ProblemDescription, verbal=True) -> bool:
    for journey in solution.journeys:
        if not utils.validate_journey(journey, problem_desc, verbal):
            return False
    logger.debug("All journeys are valid")
    return _check_requests(solution, problem_desc.requests)


def get_solution_score(solution: description.Solution, problem_desc: description.ProblemDescription):
    # keep track of total truck number and total truck distance
    total_truck_dist = 0
    for journey in solution.journeys:
        total_truck_dist += journey.length

    truck_total_num = len(solution.journeys)

    # parse constants out of problem dictionary for calculation
    truck_dist_cost = problem_desc.truck_distance_cost
    truck_cost = problem_desc.truck_cost
    truck_use_cost = problem_desc.truck_day_cost

    score = truck_total_num * (truck_cost + truck_use_cost) + total_truck_dist * truck_dist_cost

    return score


def _check_requests(solution: description.Solution, problem_requests: [description.Request]):
    """
    simple check that all requests are fulfilled
    :param solution:
    :param solution_requests:
    :return:
    """

    filled_requests = sorted(solution.get_requests())
    problem_reqs = sorted([request.id for request in problem_requests])

    return filled_requests == problem_reqs
