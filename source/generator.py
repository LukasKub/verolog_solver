import random

from utils import description
from utils import utils
from source.local_search import shaw_simmilarity
from source import validator


def _generate_null_solution() -> description.Solution:
    null_solution = description.Solution([])
    return null_solution


def generate_solution(problem_desc: description.ProblemDescription) -> description.Solution:
    """
    generates a solution, where every request is satisfied by a separate journey
    """
    journeys = []

    for request in problem_desc.requests:

        # parse constants and generate journey class
        journey_weight = request.weight
        journey_path = [1, request.location_id, 1]
        journey_length = utils.calculate_journey_length(journey_path, problem_desc)

        journey = description.Journey(journey_weight, journey_length, journey_path, [request.id])

        journeys.append(journey)
    return description.Solution(journeys)


def giant_tour_generation(problem_desc: description.ProblemDescription) -> description.Solution:
    request_list = problem_desc.requests[:]
    random.shuffle(request_list)
    return description.Solution(_get_journeys_from_request_list(request_list, problem_desc))


def shaw_generation(problem_desc: description.ProblemDescription) -> description.Solution:
    request_list = shaw_simmilarity.generate_shaw_list(problem_desc)
    return description.Solution(_get_journeys_from_request_list(request_list, problem_desc))


def advanced_shaw_generation(problem_desc: description.ProblemDescription, count=10) -> description.Solution:
    solution = shaw_generation(problem_desc)
    solution_score = validator.get_solution_score(solution, problem_desc)
    for _ in range(count):
        new_solution = shaw_generation(problem_desc)
        new_score = validator.get_solution_score(new_solution, problem_desc)

        if new_score < solution_score:
            solution = new_solution
            solution_score = new_score

    return solution


def advanced_random_generation(problem_desc: description.ProblemDescription, count=10) -> description.Solution:
    solution = giant_tour_generation(problem_desc)
    solution_score = validator.get_solution_score(solution, problem_desc)
    for _ in range(count):
        new_solution = giant_tour_generation(problem_desc)
        new_score = validator.get_solution_score(new_solution, problem_desc)

        if new_score < solution_score:
            solution = new_solution
            solution_score = new_score

    return solution


def _get_journeys_from_request_list(request_list: [description.Request], problem_desc: description.ProblemDescription) -> [description.Journey]:
    journeys = []
    cur_journey = description.Journey(0, 0, [1, 1], [])
    while request_list:     # keep going through requests until all are processed
        current_request = request_list[0]

        if cur_journey.weight + current_request.weight <= problem_desc.truck_capacity:    # only add if light enough
            new_path, new_length = utils.add_location_to_path(cur_journey.path, current_request.location_id, problem_desc)
            if new_length <= problem_desc.truck_max_distance:   # only add if short enough
                cur_journey = description.Journey(
                    cur_journey.weight + current_request.weight,
                    new_length,
                    [i for i in new_path],
                    cur_journey.requests + [current_request.id]
                )
                # remove request and continue with next one
                request_list.pop(0)
                continue

        # if I cant extend journey, add current to list and start work on new one
        journeys.append(cur_journey)
        cur_journey = description.Journey(
            current_request.weight,
            utils.calculate_journey_length([1, current_request.location_id, 1], problem_desc),
            [1, current_request.location_id, 1],
            [current_request.id]
        )
        request_list.pop(0)

    # don't forget to add last journey to list
    journeys.append(cur_journey)
    return [j for j in journeys if j.requests]
