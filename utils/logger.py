import logging
import time

write_to_file = True
# write_to_file = False

# level = logging.DEBUG
level = logging.INFO
# level = logging.ERROR
# level = logging.WARNING
if write_to_file:
    logging.basicConfig(filename="solver.log",
                        filemode="a+",
                        format="%(message)s",
                        level=level)
else:
    logging.basicConfig(level=level)


def debug(message):
    message = " ".join(["[DEBUG]", time.strftime("%H:%M:%S"), "-", str(message)])
    logging.debug(message)


def info(message):
    message = " ".join(["[INFO]", time.strftime("%H:%M:%S"), "-", str(message)])
    logging.info(message)


def warning(message):
    message = " ".join(["[WARNING]", time.strftime("%H:%M:%S"), "-", str(message)])
    logging.warning(message)


def error(message):
    message = " ".join(["[ERROR]", time.strftime("%H:%M:%S"), "-", str(message)])
    logging.error(message)
