import math
from itertools import permutations
from utils import description
from random import randrange
from functools import lru_cache
from collections import defaultdict

# ----------------------------------------------------------------------------------------------------------------------
# validation functions


def validate_journey(journey: description.Journey, problem_desc: description.ProblemDescription, verbal=True) -> bool:
    capacity_validity = check_capacity(journey.requests, problem_desc, verbal)
    length_validity = check_journey_length(journey.path, problem_desc, verbal)
    location_validity = check_journey_paths(journey, problem_desc, verbal)

    return capacity_validity and length_validity and location_validity


def check_capacity(journey_requests: [description.Request], problem_desc:description.ProblemDescription, verbal=True) -> bool:
    capacity = problem_desc.truck_capacity

    truck_occ = calculate_total_weight(journey_requests, problem_desc)

    if verbal and truck_occ > capacity:
        raise RuntimeError(f"Capacity check for Journey failed, Truck occ: {truck_occ}, capacity: {capacity}")

    return truck_occ <= capacity


def calculate_total_weight(request_list: [int], problem_desc: description.ProblemDescription) -> int:
    """
    Calculate total weight of request list
    """

    truck_occ = 0
    for req in request_list:
        truck_occ += problem_desc.requests[req - 1].weight

    return truck_occ


def check_journey_length(journey_path, problem_desc: description.ProblemDescription, verbal=True) -> bool:
    max_dist = problem_desc.truck_max_distance

    journey_length = calculate_journey_length(journey_path, problem_desc)

    if verbal and journey_length > max_dist:
        raise RuntimeError(f"Journey {journey_path} longer than max_dist, Journey len: {journey_length}, max_dist: {max_dist}")

    return journey_length <= max_dist


journey_length_dict = defaultdict(int)


def calculate_journey_length(location_path, problem_desc: description.ProblemDescription):
    path_hash = hash(tuple(location_path))
    if journey_length_dict[path_hash] == 0:
        journey_length_dict[path_hash] = _calculate_length(location_path, problem_desc)
    return journey_length_dict[path_hash]


def _calculate_length(location_path, problem_desc: description.ProblemDescription):
    locations = problem_desc.locations
    if location_path[0] != 1:
        location_path = [1] + location_path
    if location_path[len(location_path) - 1] != 1:
        location_path = location_path + [1]

    journey_distance = 0

    for i in range(len(location_path) - 1):
        leg_begin = location_path[i]
        leg_end = location_path[i+1]

        journey_distance += _get_distance(locations[leg_begin-1], locations[leg_end-1])
    return journey_distance


def get_location_distance(loc1id, loc2id, problem_desc):
    return _get_distance(problem_desc.locations[loc1id], problem_desc.locations[loc2id])


def _get_distance(loc1, loc2) -> int:
    """
    Wrapper for calculation
    Expects two locations, which have .x and .y attributes
    :return: Ceiling of Euclidean distance
    """
    return _get_distance_coor(loc1.x, loc1.y, loc2.x, loc2.y)


@lru_cache()
def _get_distance_coor(x1, y1, x2, y2) -> int:
    return math.ceil(math.sqrt((x1-x2) * (x1-x2) + (y1-y2) * (y1-y2)))


def check_journey_paths(journey: description.Journey, problem_desc: description.ProblemDescription, verbal=True) -> bool:
    for request_id in journey.requests:
        if problem_desc.requests[request_id - 1].location_id not in journey.path:
            if verbal:
                raise RuntimeError(f"Journey {journey} does not pass through all request locations")
            return False
    return True

# ----------------------------------------------------------------------------------------------------------------------
# path manipulation functions


def add_location_to_path(base_path, new_loc, problem_desc: description.ProblemDescription):
    locs = get_locations_from_path(base_path)
    locs.append(new_loc)
    return get_best_path(locs, problem_desc)


def remove_loc_from_path(base_path, old_loc, problem_desc: description.ProblemDescription):
    path_locations = get_locations_from_path(base_path)
    if not path_locations:
        raise RuntimeError(f"No locations passed to function remove_loc_from_path: {base_path}, {old_loc}")
    try:
        path_locations.remove(old_loc)
    except ValueError:
        pass
    return get_best_path(path_locations, problem_desc)


def get_locations_from_path(journey_path):
    """
    remove leading and trailing depot locations from path
    """
    return [i for i in journey_path if i != 1]


def _get_permutations(base_list):
    """
    get unique permutations from list
    """
    candidate = [[p for p in c] for c in permutations(base_list)]
    true_res = []
    for perm in candidate:
        if perm not in true_res:
            true_res.append(perm)
    return true_res


best_path_dict = defaultdict(list)


def get_best_path(location_list, problem_desc: description.ProblemDescription):
    location_list = sorted(location_list)
    loc_hash = hash(tuple(location_list))
    if not best_path_dict[loc_hash]:
        best_path_dict[loc_hash] = _get_best_path(_get_permutations(location_list), problem_desc)

    return best_path_dict[loc_hash]


def _get_best_path(possible_paths, problem_desc: description.ProblemDescription):
    """
    get best path from possible paths
    """
    path = []
    length = problem_desc.truck_max_distance * 999
    for candidate_path in possible_paths:
        candidate_path = [1] + candidate_path + [1]
        candidate_length = calculate_journey_length(candidate_path, problem_desc)
        if candidate_length <= length:
            path = candidate_path
            length = candidate_length

    return [path, length]

# ----------------------------------------------------------------------------------------------------------------------
# misc functions


def get_id(base):
    try:
        true_id = base.id
    except:
        true_id = base
    return true_id


def _generate_random_index(iterable):
    return randrange(0, len(iterable))


def generate_index(iterable, other_index=None):
    if other_index is None:
        return _generate_random_index(iterable)
    else:
        new_index = _generate_random_index(iterable)
        while new_index == other_index:
            new_index = _generate_random_index(iterable)
        return new_index


def generate_index_pair(iterable):
    first_index = generate_index(iterable)
    second_index = generate_index(iterable, first_index)
    return first_index, second_index
