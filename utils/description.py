"""
Class descriptions for this instance of verolog solver problem

Classes define more attributes than are used by solver, due to expected extensions
"""
from typing import List


class Machine:
    """
    auxiliary class for data passing
    """
    id = None
    weight = None

    def __init__(self, machine_id: int, machine_weight: int):
        self.id = machine_id
        self.weight = machine_weight

    def __str__(self):
        return f"(Machine {self.id}, " \
               f"weight = {self.weight})"

    def __repr__(self):
        return self.__str__()


class Location:
    id = None
    x = None
    y = None

    def __init__(self, location_id: int, location_x_coordinate: int, location_y_coordinate: int):
        self.id = location_id
        self.x = location_x_coordinate
        self.y = location_y_coordinate

    def __str__(self):
        return f"(Location {self.id}, " \
               f"x = {self.x}, " \
               f"y = {self.y})"

    def __repr__(self):
        return self.__str__()


class Request:
    id = None
    location_id = None
    day_from = None
    day_to = None
    machine_id = None
    machine_num = None
    weight = 999

    def __init__(self, req_id: int,
                 req_loc_id: int,
                 req_day_from: int,
                 req_day_to: int,
                 req_machine_id: int,
                 req_machine_num: int,
                 problem_desc=None):

        self.id = req_id
        self.location_id = req_loc_id
        self.day_from = req_day_from
        self.day_to = req_day_to
        self.machine_id = req_machine_id
        self.machine_num = req_machine_num
        if problem_desc is not None:
            self.weight = _calculate_request_weight(self, problem_desc)

    def __str__(self):
        return f"(Request {self.id}, " \
               f"loc id = {self.location_id}, " \
               f"machine id = {self.machine_id}, " \
               f"machine num = {self.machine_num})"

    def __repr__(self):
        return self.__str__()


class Journey:
    weight = None
    length = None
    path = None
    requests: [int] = None

    def __init__(self, journey_weight: int, journey_length: int, journey_path: [int], journey_requests: [int]):
        self.weight = journey_weight
        self.length = journey_length
        self.path = journey_path
        self.requests = journey_requests

    def __str__(self):
        return f"(Journey, " \
               f"total length = {self.length}, " \
               f"total weight = {self.weight}, " \
               f"path = {self.path}, " \
               f"requests = {self.requests})"

    def __repr__(self):
        return self.__str__()


class Solution:
    journeys: [Journey] = None

    def __init__(self, journeys: [Journey]):
        self.journeys = journeys

    def __str__(self):
        sorted_journeys = [str(j) for j in sorted(self.journeys, key=lambda itm: itm.weight)]
        return f"(Solutions, " \
               f"{len(sorted_journeys)} journeys = \n" + "\n".join(sorted_journeys) + ")"

    def __repr__(self):
        return self.__str__()

    def get_requests(self):
        requests = []
        for journey in self.journeys:
            for request in journey.requests:
                requests.append(request)

        return sorted(requests)


class ProblemDescription:
    dataset_name = None
    instance_name = None

    locations: List[Location] = []
    machines: List[Machine] = []
    requests: List[Request] = []

    truck_capacity = None
    truck_cost = None
    truck_day_cost = None
    truck_distance_cost = None
    truck_max_distance = None

    def __init__(self):
        dataset_name = None
        instance_name = None

        locations: List[Location] = []
        machines: List[Machine] = []
        requests: List[Request] = []

        truck_capacity = None
        truck_cost = None
        truck_day_cost = None
        truck_distance_cost = None
        truck_max_distance = None

    def __str__(self):
        return    f"Dataset: {self.dataset_name}\n"      \
                  f"Instance: {self.instance_name}\n"    \
                  f"Locations:\n" \
                  f"\t" + "\n\t".join([str(loc) for loc in self.locations]) + f"\n"       \
                  f"Machines:\n" \
                  f"\t" + "\n\t".join([str(mach) for mach in self.machines]) + f"\n"         \
                  f"Requests:\n" \
                  f"\t" + "\n\t".join([str(req) for req in self.requests]) + f"\n"         \
                  f"Truck:\n"                           \
                  f"\tCapacity: {self.truck_capacity}\n" \
                  f"\tMax distance: {self.truck_max_distance}\n"\
                  f"\tCost: {self.truck_cost}\n"         \
                  f"\tDay cost: {self.truck_day_cost}\n" \
                  f"\tDistance cost: {self.truck_distance_cost}\n"

    def __repr__(self):
        return self.__str__()


def _calculate_request_weight(request: Request, problem_desc: ProblemDescription) -> int:
    machines = problem_desc.machines
    machine_id = request.machine_id
    machine_count = request.machine_num

    machine_weight = machines[machine_id - 1].weight

    return machine_count * machine_weight
