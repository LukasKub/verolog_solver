import os
import argparse

from benchmarks import generator_benchmarks
from benchmarks import generation_plotter
from benchmarks import optimization_benchmarks
from benchmarks import optimization_plotter


def run_opt_benchmarks(file_path):
    if os.path.isdir(file_path):
        for file_name in sorted(os.listdir(file_path)):
            run_opt_benchmarks(file_path + file_name)
    else:
        optimization_benchmarks.run_all_optimization_benchmarks(file_path)


def run_gen_benchmarks(file_path):
    if os.path.isdir(file_path):
        for file_name in sorted(os.listdir(file_path)):
            run_gen_benchmarks(file_path + file_name)
    else:
        generator_benchmarks.run_all_generation_benchmarks(file_path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Script for solving a VeRoLog Challenge 2019 instance')
    parser.add_argument('path', metavar='path', type=str,
                        help='The file containing the instance or folder containing instance files')
    parser.add_argument('--opt', dest='opt', action='store_true',
                        help='Run the optimization benchmarks')
    parser.add_argument('--gen', dest='gen', action='store_true',
                        help='Run the generation benchmarks')

    args = parser.parse_args()

    if args.opt:
        run_opt_benchmarks(args.path)
        optimization_plotter.save_individual_plots()
        optimization_plotter.save_overall_plot()

    if args.gen:
        run_gen_benchmarks(args.path)
        generation_plotter.save_plot()
